-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 03, 2018 at 08:14 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `syscad`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(1, 'Nature Discovery Center'),
(2, 'Local Foods'),
(3, 'Aspen Heights Parking'),
(4, 'Willowbrook Center'),
(5, 'Wall Plates'),
(6, 'Electrical Refeed'),
(7, 'El Franco Lee Tower'),
(8, 'UTMB East bridge'),
(9, 'Tamu System Building'),
(10, 'Eagle Traces'),
(11, 'UTMB Health Center'),
(12, 'Project Snap');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(6) NOT NULL,
  `image_name` varchar(150) DEFAULT NULL,
  `image_path` varchar(160) DEFAULT NULL,
  `image_type` varchar(25) DEFAULT NULL,
  `image_size` varchar(25) DEFAULT NULL,
  `image_width` int(4) DEFAULT NULL,
  `image_height` int(4) DEFAULT NULL,
  `thumbnail` varchar(250) DEFAULT NULL,
  `category` varchar(15) DEFAULT NULL,
  `date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `image_name`, `image_path`, `image_type`, `image_size`, `image_width`, `image_height`, `thumbnail`, `category`, `date`) VALUES
(1, 'project_snap.png', 'uploads/project_snap.png', 'image/png', '3463 KB', 5000, 2302, 'uploads/thumbnail/project_snap.png', 'Project Snap', '2018-07-01'),
(2, 'project_snap1.png', 'uploads/project_snap1.png', 'image/png', '1035 KB', 5000, 2302, 'uploads/thumbnail/project_snap1.png', 'Project Snap', '2018-07-01'),
(3, 'project_snap2.png', 'uploads/project_snap2.png', 'image/png', '4251 KB', 5000, 2302, 'uploads/thumbnail/project_snap2.png', 'Project Snap', '2018-07-01'),
(4, 'project_snap3.png', 'uploads/project_snap3.png', 'image/png', '4735 KB', 5000, 2302, 'uploads/thumbnail/project_snap3.png', 'Project Snap', '2018-07-01'),
(5, 'project_snap4.png', 'uploads/project_snap4.png', 'image/png', '4464 KB', 5000, 2302, 'uploads/thumbnail/project_snap4.png', 'Project Snap', '2018-07-01'),
(6, 'aspen-heights-parking.png', 'uploads/aspen-heights-parking.png', 'image/png', '1774 KB', 5000, 2302, 'uploads/thumbnail/aspen-heights-parking.png', 'Aspen Heights P', '2018-07-01'),
(7, 'aspen-heights-parking1.png', 'uploads/aspen-heights-parking1.png', 'image/png', '1786 KB', 5000, 2302, 'uploads/thumbnail/aspen-heights-parking1.png', 'Aspen Heights P', '2018-07-01'),
(8, 'aspen-heights-parking2.png', 'uploads/aspen-heights-parking2.png', 'image/png', '1538 KB', 5000, 2302, 'uploads/thumbnail/aspen-heights-parking2.png', 'Aspen Heights P', '2018-07-01'),
(9, 'eagle_traces.png', 'uploads/eagle_traces.png', 'image/png', '919 KB', 5000, 2302, 'uploads/thumbnail/eagle_traces.png', 'Eagle Traces', '2018-07-01'),
(10, 'eagle_traces1.png', 'uploads/eagle_traces1.png', 'image/png', '2593 KB', 5000, 2302, 'uploads/thumbnail/eagle_traces1.png', 'Eagle Traces', '2018-07-01'),
(11, 'eagle_traces2.png', 'uploads/eagle_traces2.png', 'image/png', '1833 KB', 5000, 2302, 'uploads/thumbnail/eagle_traces2.png', 'Eagle Traces', '2018-07-01'),
(12, 'electrical-refeed.png', 'uploads/electrical-refeed.png', 'image/png', '2093 KB', 5000, 2302, 'uploads/thumbnail/electrical-refeed.png', 'Electrical Refe', '2018-07-01'),
(13, 'electrical-refeed1.png', 'uploads/electrical-refeed1.png', 'image/png', '978 KB', 5000, 2302, 'uploads/thumbnail/electrical-refeed1.png', 'Electrical Refe', '2018-07-01'),
(14, 'electrical-refeed2.png', 'uploads/electrical-refeed2.png', 'image/png', '1825 KB', 5000, 2302, 'uploads/thumbnail/electrical-refeed2.png', 'Electrical Refe', '2018-07-01'),
(15, 'electrical-refeed3.png', 'uploads/electrical-refeed3.png', 'image/png', '1730 KB', 5000, 2302, 'uploads/thumbnail/electrical-refeed3.png', 'Electrical Refe', '2018-07-01'),
(16, 'el-franco-lee-tower.png', 'uploads/el-franco-lee-tower.png', 'image/png', '2302 KB', 5000, 2302, 'uploads/thumbnail/el-franco-lee-tower.png', 'El Franco Lee T', '2018-07-01'),
(17, 'el-franco-lee-tower1.png', 'uploads/el-franco-lee-tower1.png', 'image/png', '2151 KB', 5000, 2302, 'uploads/thumbnail/el-franco-lee-tower1.png', 'El Franco Lee T', '2018-07-01'),
(18, 'el-franco-lee-tower2.png', 'uploads/el-franco-lee-tower2.png', 'image/png', '2441 KB', 5000, 2302, 'uploads/thumbnail/el-franco-lee-tower2.png', 'El Franco Lee T', '2018-07-01'),
(19, 'el-franco-lee-tower3.png', 'uploads/el-franco-lee-tower3.png', 'image/png', '2406 KB', 5000, 2302, 'uploads/thumbnail/el-franco-lee-tower3.png', 'El Franco Lee T', '2018-07-01'),
(20, 'el-franco-lee-tower4.png', 'uploads/el-franco-lee-tower4.png', 'image/png', '66 KB', 1540, 713, 'uploads/thumbnail/el-franco-lee-tower4.png', 'El Franco Lee T', '2018-07-01'),
(21, 'local-foods.png', 'uploads/local-foods.png', 'image/png', '3794 KB', 5000, 2302, 'uploads/thumbnail/local-foods.png', 'Local Foods', '2018-07-01'),
(22, 'local-foods1.png', 'uploads/local-foods1.png', 'image/png', '3048 KB', 5000, 2302, 'uploads/thumbnail/local-foods1.png', 'Local Foods', '2018-07-01'),
(23, 'local-foods2.png', 'uploads/local-foods2.png', 'image/png', '3489 KB', 5000, 2302, 'uploads/thumbnail/local-foods2.png', 'Local Foods', '2018-07-01'),
(24, 'local-foods3.png', 'uploads/local-foods3.png', 'image/png', '3949 KB', 5000, 2302, 'uploads/thumbnail/local-foods3.png', 'Local Foods', '2018-07-01'),
(25, 'local-foods4.png', 'uploads/local-foods4.png', 'image/png', '3949 KB', 5000, 2302, 'uploads/thumbnail/local-foods4.png', 'Local Foods', '2018-07-01'),
(26, 'nature-discovery.png', 'uploads/nature-discovery.png', 'image/png', '1780 KB', 5000, 2302, 'uploads/thumbnail/nature-discovery.png', 'Nature Discover', '2018-07-01'),
(27, 'tamu_system_building.png', 'uploads/tamu_system_building.png', 'image/png', '2801 KB', 5000, 2302, 'uploads/thumbnail/tamu_system_building.png', 'Tamu System Bui', '2018-07-01'),
(28, 'tamu_system_building1.png', 'uploads/tamu_system_building1.png', 'image/png', '4637 KB', 5000, 2302, 'uploads/thumbnail/tamu_system_building1.png', 'Tamu System Bui', '2018-07-01'),
(29, 'utmb_east_bridge.png', 'uploads/utmb_east_bridge.png', 'image/png', '2474 KB', 5000, 2302, 'uploads/thumbnail/utmb_east_bridge.png', 'UTMB East bridg', '2018-07-01'),
(30, 'utmb_east_bridge1.png', 'uploads/utmb_east_bridge1.png', 'image/png', '2324 KB', 5000, 2302, 'uploads/thumbnail/utmb_east_bridge1.png', 'UTMB East bridg', '2018-07-01'),
(31, 'utmb_health_center.png', 'uploads/utmb_health_center.png', 'image/png', '5132 KB', 5000, 2302, 'uploads/thumbnail/utmb_health_center.png', 'UTMB Health Cen', '2018-07-01'),
(32, 'utmb_health_center1.png', 'uploads/utmb_health_center1.png', 'image/png', '5503 KB', 5000, 2302, 'uploads/thumbnail/utmb_health_center1.png', 'UTMB Health Cen', '2018-07-01'),
(33, 'utmb_health_center2.png', 'uploads/utmb_health_center2.png', 'image/png', '3233 KB', 5000, 2303, 'uploads/thumbnail/utmb_health_center2.png', 'UTMB Health Cen', '2018-07-01'),
(34, 'utmb_health_center3.png', 'uploads/utmb_health_center3.png', 'image/png', '1658 KB', 5000, 2303, 'uploads/thumbnail/utmb_health_center3.png', 'UTMB Health Cen', '2018-07-01'),
(35, 'utmb_health_center4.png', 'uploads/utmb_health_center4.png', 'image/png', '5636 KB', 5000, 2302, 'uploads/thumbnail/utmb_health_center4.png', 'UTMB Health Cen', '2018-07-01'),
(36, 'utmb_health_center5.png', 'uploads/utmb_health_center5.png', 'image/png', '6634 KB', 5000, 2301, 'uploads/thumbnail/utmb_health_center5.png', 'UTMB Health Cen', '2018-07-01'),
(37, 'wall-plates.png', 'uploads/wall-plates.png', 'image/png', '149 KB', 1875, 864, 'uploads/thumbnail/wall-plates.png', 'Wall Plates', '2018-07-01'),
(38, 'willowbrook-medical.png', 'uploads/willowbrook-medical.png', 'image/png', '2708 KB', 5000, 2302, 'uploads/thumbnail/willowbrook-medical.png', 'Willowbrook Cen', '2018-07-01'),
(39, 'willowbrook-medical1.png', 'uploads/willowbrook-medical1.png', 'image/png', '2483 KB', 5000, 2302, 'uploads/thumbnail/willowbrook-medical1.png', 'Willowbrook Cen', '2018-07-01');

-- --------------------------------------------------------

--
-- Table structure for table `logoimage`
--

CREATE TABLE `logoimage` (
  `id` int(11) NOT NULL,
  `path` varchar(150) NOT NULL,
  `image_name` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logoimage`
--

INSERT INTO `logoimage` (`id`, `path`, `image_name`) VALUES
(1, 'assets/img/logo/logo.jpg', 'logo.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `title`
--

CREATE TABLE `title` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `title`
--

INSERT INTO `title` (`id`, `name`) VALUES
(1, 'SysCAD Engineering Pvt Ltd');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `emp_id` varchar(15) NOT NULL,
  `username` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `type` varchar(10) NOT NULL,
  `email` varchar(50) NOT NULL,
  `security_txt` varchar(20) NOT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `emp_id`, `username`, `password`, `type`, `email`, `security_txt`, `status`) VALUES
(1, 'UID001', 'Raushan', '827ccb0eea8a706c4c34a16891f84e7b', 'Admin', 'raushankmr10@gmail.com', '1234', 'Approve');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logoimage`
--
ALTER TABLE `logoimage`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `title`
--
ALTER TABLE `title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `logoimage`
--
ALTER TABLE `logoimage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `title`
--
ALTER TABLE `title`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
