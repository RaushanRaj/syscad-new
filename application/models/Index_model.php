<?php
class Index_model extends CI_Model {
    
	public function selectmaxidfromuser()
	{
		$this->db->select_max('id');
		$query = $this->db->get('users');
		return $query->row_array();
	}
	
	public function insert($userData)
	{
		return $this->db->insert('users', $userData);
	}
	
	public function email_check($email)
	{
	  $this->db->select('*');
	  $this->db->from('users');
	  $this->db->where('email',$email);
	  $query = $this->db->get();
	  if($query->num_rows()>0){
		return false;
	  }else{
		return true;
	  }
	}
	public function login_user($email,$pass)
	{
	  $this->db->select('*');
	  $this->db->from('users');
	  $this->db->where('email',$email);
	  $this->db->where('password',$pass);
	  if($query = $this->db->get())
	  {
		  return $query->row_array();
	  }
	  else{
		return false;
	  }
	}
	public function portfolio()
	{
		$query = $this->db->get('images',21);
		return $query->result_array();
	}
	public function select_index_portfolio()
	{
		$query = $this->db->get('images', 8);
		return $query->result_array();
	}

	public function record_count() {
        return $this->db->count_all("images");
    }
	
	
}

?>