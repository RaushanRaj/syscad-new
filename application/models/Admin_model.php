<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends CI_Model {

	public function __construct()
	{
	  parent::__construct();
	  
	}
	public function count_upload_images()
	{
		return $this->db->get('images')->num_rows();
	}
	public function insertcategory($category)
	{
		return $this->db->insert('category', $category);
	}
	public function selectcategory()
	{
		$query = $this->db->get('category');
		return $query->result_array();
	}
	public function countcategory()
	{
		return $this->db->get('category')->num_rows();
	}
	public function selectmaxidfromimage()
	{
		$this->db->select_max('id');
		$query = $this->db->get('images');
		return $query->row_array();
	}
	public function get_duplicate_category($data)
	{
		$this->db->where('name', $data);
		return $this->db->get('category')->num_rows();
	}
	public function insertimages($data)
	{
		return $this->db->insert('images', $data);
	}
	public function selectusers()
	{
		$query = $this->db->get('users');
		return $query->result_array();
	}
	public function countusers()
	{
		return $this->db->get('users')->num_rows();
	}
	public function get_userdata($email,$id)
	{
	  $this->db->select('*');
	  $this->db->from('users');
	  $this->db->where('email',$email);
	  $this->db->where('id',$id);
	  if($query = $this->db->get())
	  {
		  return $query->result_array();
	  }
	  else{
		return false;
	  }
	} 
	public function update_user_status($id, $data)
	{
	  $this->db->where('id', $id);
      return $this->db->update('users', $data);
	}
	
	/*Logo image Coding */
	public function insertlogoimage($logoimage)
	{
		return $this->db->insert('logoimage', $logoimage);
	}
	public function updatelogoimage($logoimage)
	{
		return $this->db->update('logoimage', $logoimage);
	}
	public function count_logo_image()
	{
		return $this->db->get('logoimage')->num_rows();
	}

	/*...............Title Details............... */
	public function sql_insert_title($title)
	{
		return $this->db->insert('title', $title);
	}
	public function sql_update_title($title)
	{
		return $this->db->update('title', $title);
	}
	public function count_title()
	{
		return $this->db->get('title')->num_rows();
	}

	/*...............Check old password avaialable............... */
	public function update_password($password){
		return $this->db->update('users', $password);
	}
	public function check_old_password($email, $password){
		   $this->db->select('*');
		   $this->db->where('email', $email);  
		   $this->db->where('password', $password);  
		   return $this->db->get("users")->num_rows();
	}

}
?>
