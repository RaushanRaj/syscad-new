<body class="nav-md">
    <!-- page content -->
        <div class="right_col" role="main">
		<div class="row">
		  <div class="col-sm-12 col-md-12 col-xs-12">
		    <div class="x_panel">
			      <div class="x_title">
                    <h2>Add Category</h2>
                    <div class="clearfix"></div>
                  </div>
				   <div class="x_content">
				    <form method="post" action="<?php echo base_url(); ?>admin/add_category" class="form-horizontal form-label-left">
				   <?php
					if($this->session->flashdata('item')){
					$message = $this->session->flashdata('item');
					?>
					<div class="<?php echo $message['class'] ?>"><?php echo $message['message']; ?>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
					</button>
					</div>
					<?php }?>
					   <div class="form-group">
                        <div class="col-sm-3 col-md-6 col-sm-6 col-xs-12">
						<label for="category">Category :<span class="text-danger">*</span></label>
						<input type="text" name="category" class="form-control"  autofocus required="required"/>
						<span style="color:red"><?php echo form_error('category'); ?></span>
						</div>
                      </div>
					  <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-xs-12 col-sm-12 col-md-6 ">
						  <input type="submit" name="submit" value="Submit" class="btn btn-primary col-xs-12 col-sm-6 col-md-3 "/>
						  <a href="<?php echo base_url(); ?>admin/viewcategory" class="btn btn-primary pull-right">View Category</a>
                        </div>
                      </div>
					</form>
				   </div>
			  </div>
		  </div>
		</div>
		</div>
		