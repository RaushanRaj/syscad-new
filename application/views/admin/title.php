
<body class="nav-md">
    <!-- page content -->
        <div class="right_col" role="main">
		<div class="row">
		  <div class="col-sm-12 col-md-12 col-xs-12">
		    <div class="x_panel">
			      <div class="x_title">
                    <h2>Add Website Title</h2>
                    <div class="clearfix"></div>
                  </div>
				   <div class="x_content">
				   <?php
					if($this->session->flashdata('item')){
					$message = $this->session->flashdata('item');
					?>
					<div class="<?php echo $message['class'] ?>"><?php echo $message['message']; ?>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					</div>
					<?php }?>
				    <form method="post" action="<?php echo base_url(); ?>admin/insert_title" class="form-horizontal form-label-left" enctype="multipart/form-data">
					   
					   <div class="form-group">
                        <div class="col-sm-3 col-md-6 col-sm-6 col-xs-12">
						<label for="title">Title :<span class="text-danger">*</span></label>
						<input type="text" name="title" class="form-control" required="required" autofocus />
						<span style="color:red"><?php echo form_error('title'); ?></span>
						</div>
                      </div>
					  <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-sm-3 col-md-6 col-sm-6 col-xs-12">
						  <input type="submit" name="submit" value="Submit" class="btn btn-primary col-xs-12 col-sm-6 col-md-3 "/>
                        </div>
                      </div>
					</form>
				   </div>
			  </div>
		  </div>
		</div>
		</div>
		