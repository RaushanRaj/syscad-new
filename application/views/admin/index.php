  <body class="nav-md">
    <!-- page content -->
        <div class="right_col" role="main">
	<!--. Widget design -->
		  <div class="row">
              <div class="col-md-12">
                <div class="">
                  <div class="x_content">
				  <div class="row">
                      <div class="row">
                      <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
						<a href="<?php echo base_url(); ?>admin/upload">
                          <div class="icon"><i class="fa fa-upload"></i>
                          </div>
                          <div class="count"><?php echo $upload_images;?></div>

                          <h3>Upload</h3>
                          <p></p>
						  </a>
                        </div>
                      </div>
                      <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
						<a href="<?php echo base_url(); ?>admin/account">
                          <div class="icon"><i class="fa fa-group"></i>
                          </div>
                          <div class="count"><?php echo $users;?></div>

                          <h3>Users</h3>
                          <p></p>
						  </a>
                        </div>
                      </div>
                    </div>
                    </div>
				  </div>
				</div>
		    </div>
		</div><!--. Widget design -->
		</div><!-- page content -->
