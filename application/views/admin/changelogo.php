<script>
function hide()
		  {
			 document.getElementById('uploadPreview').style.display = 'none';
		  }
	 function PreviewImage() {
		 document.getElementById('uploadPreview').style.display = 'inline';
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("uploadImage").files[0]);

        oFReader.onload = function (oFREvent) {
            document.getElementById("uploadPreview").src = oFREvent.target.result;
        };
    }; 
	
  </script>
<body class="nav-md" onload="hide();">
    <!-- page content -->
        <div class="right_col" role="main">
		<div class="row">
		  <div class="col-sm-12 col-md-12 col-xs-12">
		    <div class="x_panel">
			      <div class="x_title">
                    <h2>Change logo images</h2>
                    <div class="clearfix"></div>
                  </div>
				   <div class="x_content">
				    <form id="myForm" method="post" action="<?php echo base_url(); ?>admin/changelogo" class="form-horizontal form-label-left" enctype="multipart/form-data">
					 <?php if(!empty($error)){?>
					  <?php echo $error;?>
					  <?php }?>
					<?php
					if($this->session->flashdata('item')){
					$message = $this->session->flashdata('item');
					?>
					<div class="<?php echo $message['class'] ?>"><?php echo $message['message']; ?>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					</div>
					<?php }?>
					   <div class="form-group">
                        <div class="col-sm-3 col-md-6 col-sm-6 col-xs-12" >
						<img id="uploadPreview" width="291" height="105" />
                        </div>
                      </div>
					   <div class="form-group">
                        <div class="col-sm-3 col-md-6 col-sm-6 col-xs-12">
						<span class="pull-left">Logo width * height : 291 * 105</span>
                        <input id="uploadImage" type="file" name="image" onchange="PreviewImage();" class="form-control col-md-7 col-xs-12" accept="image/gif, image/jpeg, image/png, image/jpg" required="required"/>
						<span style="color:red"><?php echo form_error('image'); ?></span>
						</div>
                      </div>
					  <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-sm-3 col-md-6 col-sm-6 col-xs-12">
						  <input type="submit" name="submit" value="Submit" class="btn btn-primary col-xs-12 col-sm-6 col-md-3 "/>
                        </div>
                      </div>
					</form>
				   </div>
			  </div>
		  </div>
		</div>
		</div>
		