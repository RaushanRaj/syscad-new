<body class="nav-md">
<!-- page content -->
        <div class="right_col" role="main">
		<div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
				  <div class="x_title">
				   <h2>View Category</h2>
				   <div class="clearfix"></div>
				  </div>
				   <div class="x_content">
				   <div class="table-responsive">
					 <table class="table table-hover">
					   <thead  id="trbackground">
							<tr>
							  <th>SNo</th>
							  <th>Category</th>
							  <!--.<th>Delete</th>-->
							</tr>
						  </thead>
						   <tbody>
						   <?php
                             $i = 1; $j = 0;
							 while($i <= $count){
								 $arr[] = $i;
								 $i++;
							 }
						   ?>
						    <?php if($count) {?>
						  <?php foreach($value as $row){?>
						   <tr>
						    <td><?php echo $arr[$j]; ?> </td>
						   <td><?php echo $row['name']; ?> </td>
						   </tr>
							<?php $j++; } } else {?>
							<tr class="text-center">
							 <td colspan="4">No Record found.</td>
							 </tr>
							<?php }?>
						   </tbody>
					 </table>
				 </div>
				   </div>
				</div>
				<div class="x_content">
				<a href="<?php echo base_url(); ?>admin/category" class="btn btn-primary">Add Category</a>
				</div>
			</div>
		</div>
		</div>