<?php
if($this->session->user_data['type'] != 'Admin'){
  redirect('home/index');
}
 ?>
 <!DOCTYPE html>
<html lang="en">
  <head>
  <title>SysCAD Engineering Pvt Ltd</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon.png" type="image/png" />
     <!-- custom style -->
    <link href="<?php echo base_url(); ?>assets/vendors/style/style.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="<?php echo base_url(); ?>assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url(); ?>assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress 
    <link href="<?php echo base_url(); ?>assets/vendors/nprogress/nprogress.css" rel="stylesheet">-->
    <!-- iCheck 
    <link href="<?php echo base_url(); ?>assets/vendors/iCheck/skins/flat/green.css" rel="stylesheet">-->
    <!-- bootstrap-progressbar 
    <link href="<?php echo base_url(); ?>assets/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">-->
    <!-- JQVMap 
    <link href="<?php echo base_url(); ?>assets/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>-->
    <!-- bootstrap-daterangepicker 
    <link href="<?php echo base_url(); ?>assets/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">-->
    <!-- Custom Theme Style -->
    <link href="<?php echo base_url(); ?>assets/build/css/custom.min.css" rel="stylesheet">
  <!-- Add fancyBox -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/source/jquery.fancybox.css" />
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/vendors/js/jquery-1.11.1.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/source/jquery.fancybox.pack.js"></script>
  <script type="text/javascript">
   $(".fancybox").fancybox({
    openEffect  : 'none',
    closeEffect : 'none',
    helpers : {
      media : {}
    }
   }); 
</script>
  </head>
<div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
            <span class="site_title" id="font-white"><a href="<?php echo base_url(); ?>admin/index">Dashboard</a></span>
			</div>
             <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo base_url(); ?>assets/images/user.png" alt="" class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2><?php echo $this->session->user_data['username']; ?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->
            <div class="clearfix"></div>
            <br />
            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
               <h3>Menu</h3>
                <ul class="nav side-menu">
                  <li><a href="<?php echo base_url(); ?>admin/admin_search"><i class="fa fa-search"></i> Search </a></li>
                  <li><a href="<?php echo base_url(); ?>admin/upload"><i class="fa fa-upload"></i> Porfolio page </a></li>
                  <li><a href="<?php echo base_url(); ?>admin/service"><i class="fa fa-upload"></i> Services page </a></li>
                  <li><a><i class="fa fa-file"></i>Categories <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li class="sub_menu"><a href="<?php echo base_url(); ?>admin/category">Add Category</a></li>
                            <li><a href="<?php echo base_url(); ?>admin/viewcategory">View Category</a></li>
                          </ul>
                  </li>
                  <li><a href="<?php echo base_url(); ?>admin/account"><i class="fa fa-users"></i> Users </a></li>
				  <li><a><i class="fa fa-cog"></i>Settings <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
					  <li><a href="<?php echo base_url(); ?>admin/changelogoimage">Change Logo Image</a></li>
					  <li><a href="<?php echo base_url(); ?>admin/announcement">Add Announcement</a></li>
					  <li><a href="<?php echo base_url(); ?>admin/title">Add Website Title</a></li>
					  <li><a href="<?php echo base_url(); ?>admin/change_password">Change password</a></li>
                    </ul>
                  </li>
				  <li><a href="<?php echo base_url(); ?>admin/logout"><i class="fa fa-sign-out"></i>Logout</a></li>
                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="<?php echo base_url(); ?>admin/logout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>
              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo base_url(); ?>assets/images/user.png" alt=""><?php echo $this->session->user_data['username']; ?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="javascript:;"> </a></li>
                    <li><a href="<?php echo base_url(); ?>admin/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->