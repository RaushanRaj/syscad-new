<script>
         /* hide submit button , image upload and file id field. */
		 function hide()
		  {
			  document.getElementById("Submit").disabled = true;
			  document.getElementById('file-id').style.display = 'none';
		  }
		  
		  /* Enable sumbit button when field is not empty */
		  function enableSubmit()
		  {	
			if(document.getElementById('files').value == ""  || document.getElementById('category').value == "" || document.getElementById('tag').value == "")
			{
				document.getElementById("Submit").disabled = true;
			}
			else
			{
				document.getElementById("Submit").disabled = false;
			}
		   }
		 /* field is on change when field is select */
		$(document).ready(function(){
			$('input#files').change(function()
			{
				enableSubmit();
			});
			$('select#category').change(function()
			{
				enableSubmit();
			});
			$('textarea#tag').keyup(function()
			{
				enableSubmit();
			});
		});
		
		/* file select on change  */
		  $(document).ready(function(){
			  $('input#files').change(function()
			  {
				 var filename = $("#files").val();
				var ext = filename.substr((filename.lastIndexOf('.') + 1)).toLowerCase();
				if(ext == "jpg" || ext == "jpeg" || ext == "png" || ext == "gif")
				{
					document.getElementById('imageupload').style.display = 'none';
				}
				else{
					document.getElementById('imageupload').style.display = 'inline';
                     $('input#image').attr('required', true)					
				}
			  });
		  });
		  
		  /* Show the thumnail when image is select. */
		  function readURL(input) {
           if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#thumnailimage')
                    .attr('src', e.target.result)
                    .width(500)
                    .height(300);
            };
            reader.readAsDataURL(input.files[0]);
          }
        }
	 /* Get the file details on file select. */
    $(document).ready(function(){
		 var ext1,ext2;
		 var fname1,fname2,html;
        $('input[type="file"]').change(function(e){
            var fileName = e.target.files[0].name;
			var size     = e.target.files[0].size;
			var type     = e.target.files[0].type;
			    ext2   = (fileName.split('.').pop()).toLowerCase();
		        html = "<br/><b>Name: </b>"+ fileName + "<br/><b>Size: </b>"+ Math.round(size/1024) + "KB" + "<br/><b>Type: </b>"+ type + "<br/><b>Extension: </b>"+ext2 + "";
		   
			document.getElementById("imgdetails").innerHTML = html;
        }); 
    });
		
/*Get the file id on type select. */
	$(document).ready(function(){
		 $('#category').change(function(){
			  var filevalue = $("#category").val();
	<?php 
	   if($id['id'] == "")
	   {
		   $file_id = 1;
	   }
	   else{
			 $file_id = $id['id'] + 1;
	   }
	?>
    var d = new Date();
    var currentyear = d.getFullYear();
    var twovalue = filevalue.substring(0, 2).toUpperCase();
	var newfilevalue = twovalue + currentyear + <?php echo $file_id; ?>;
			document.getElementById('file-id').style.display = 'inline';
		    document.getElementById('fileid').value = newfilevalue; 
		  }); 
    });
	
	/*Loader  */
	 $(document).ready(function(){
		$('#myForm').submit(function() {
		 $('#loaderImg').show(); 
		  return true;
		});
	});
	
</script>
<body class="nav-md" onload="hide();">
    <!-- page content -->
        <div class="right_col" role="main">
		<div class="row">
		<div class="x_panel">
		 <div class="col-xs-12 col-sm-12 col-md-6">
		  <div class="x_title">
			<h2>Upload</h2>
			<div class="clearfix"></div>
		  </div>
		  <div class="x_content">
		  <?php if(!empty($error)){?>
		  <?php echo $error;?>
		  <?php }?>
		  
		  <?php
			if($this->session->flashdata('item')){
			$message = $this->session->flashdata('item');
			?>
			<div class="<?php echo $message['class'] ?>"><?php echo $message['message'];?>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
			</button>
			</div>
			<?php }?>
		   <form method="post" action="<?php echo base_url(); ?>admin/uploadfile" class="form-horizontal form-label-left" enctype="multipart/form-data" id="myForm">
		   <div id="file-id">
				<div class="form-group">
					<div class="col-xs-12 col-sm-12 col-md-12">
					<label>File ID:</label>
					<input type="text" id="fileid" name="fileid"  class="form-control col-xs-12 col-sm-12 col-md-12" readonly="readonly"/>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-xs-12 col-sm-12 col-md-12">
				<label>Upload File:</label>
				<input type="file" id="files" name="files"  class="form-control col-xs-12 col-sm-12 col-md-12 " onchange="readURL(this);" required="required"/>
				<span style="color:red"><?php echo form_error('files'); ?>
				<div id="filespace"></div>
				</div>
		    </div>
			  <div class="form-group">
				<div class="col-xs-12 col-sm-12 col-md-12">
				<label>Select Image Category:</label>
				<select class="form-control col-xs-12 col-sm-12 col-md-12" name="category" id="category" required="required">
						<option value="" selected disabled>Select Category</option>
						<?php foreach($category as $row){ ?>
						<option value="<?php echo $row['name']; ?>"><?php echo $row['name']; ?></option>
						<?php }?>
					</select>
				<span style="color:red"><?php echo form_error('category'); ?>
				</div>
			  </div>
			   <div class="form-group">
				<div class="col-xs-12 col-sm-12 col-md-12">
				<textarea class="form-control col-xs-12 col-sm-12 col-md-12" name="tag" id="tag" rows="3" placeholder="Enter key words for searching this image" required="required" ></textarea>
				<span style="color:red"><?php echo form_error('tag'); ?>   
				</div>
			  </div>
			  <div class="ln_solid"></div>
			  <div class="form-group">
				<div class="col-xs-12 col-sm-12 col-md-6">
				  <input type="submit" id="Submit" name="Submit" value="Submit" class="btn btn-primary col-xs-12 col-sm-12 col-md-12" onclick="this.value='Uploading, please wait...';"/>
				</div>
			  </div>
		   </form>
		  </div>
		 </div>
		 <div class="col-xs-12 col-sm-12 col-md-6">
			 <div class="x_content">
			       <div class="form-group">
                        <div class="col-xs-12 col-sm-12 col-md-12">
						<img id="thumnailimage"/>
						<div id="imgdetails"></div>
                        </div>
                    </div>
			 </div>
		 </div>
		 <img src="<?php echo base_url(); ?>assets/img/loader/loading.gif" alt="loader1" style="display:none;"  id="loaderImg" class="loader">
		</div>
		</div>
	<!--. Widget design -->
	    </div>