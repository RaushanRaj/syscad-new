<script>
  function hide()
		  {
			 document.getElementById("Submit").disabled = true;
		  }
		
		
		function enableSubmit()
		 {	
			if(document.getElementById('userType').value == "" || document.getElementById('userStatus').value == "" )
			{
				document.getElementById("Submit").disabled = true;
			}
			else
			{
				document.getElementById("Submit").disabled = false;
			}
		 }
		
		$(document).ready(function(){
			$('select#userType').change(function()
			{
				enableSubmit();
			});
			$('select#userStatus').change(function()
			{
				enableSubmit();
			});
		});
  </script>
<body class="nav-md" onload="hide();">
<!-- page content -->
  <div class="right_col" role="main">
<div class="row">
	<div class="col-sm-12 col-md-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
			<h2>Update User Details</h2>
			<div class="clearfix"></div>
			</div>
			
			<?php
			if($this->session->flashdata('item')){
			$message = $this->session->flashdata('item');
			?>
			<div class="<?php echo $message['class'] ?>"><?php echo $message['message'];?>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
			</button>
			</div>
			<?php }?>
			
			<div class="x_content">
			<form method="post" action="<?php echo base_url(); ?>admin/update_user_status" class="form-horizontal form-label-left">
			<?php foreach($value as $row){ ?>
			    <div class="form-group" style="display:none;">
				<input type="text" id="id" name="id" class="form-control col-md-7 col-xs-12" value="<?php echo $row['id']; ?>">
				</div>
				<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12" for="empid">Emp ID :</label>
				<div class="col-md-6 col-sm-6 col-xs-12">
				  <input type="text" id="empid" name="empid" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $row['emp_id']; ?>" readonly>
				</div>
				</div>
				<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name :</label>
				<div class="col-md-6 col-sm-6 col-xs-12">
				  <input type="text" id="username" name="username" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $row['username']; ?>" readonly>
				</div>
				</div>
				<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type">Type :</label>
				<div class="col-md-6 col-sm-6 col-xs-12">
				  <select name="userType" id="userType" class="form-control col-md-7 col-xs-12">
					   <option value="Admin" <?php if($row['type'] == "Admin") echo 'selected'?>>Admin</option>
					   <option value="User" <?php if($row['type'] == "User") echo 'selected'?>>User</option>
					   <option value="Uploader" <?php if($row['type']=="Uploader") echo 'selected'?>>Uploader</option>
					   </select>
				</div>
				</div>
				<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email :</label>
				<div class="col-md-6 col-sm-6 col-xs-12">
				  <input type="text" id="email" name="email" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $row['email']; ?>" readonly>
				</div>
				</div>
				<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12" for="status">Status :</label>
				<div class="col-md-6 col-sm-6 col-xs-12">
				   <select name="userStatus" id="userStatus" class="form-control col-md-7 col-xs-12">
					<option value="Pending" <?php if($row['status']=="Pending") echo 'selected'?>>Pending</option>
					<option value="Approve" <?php if($row['status']=="Approve") echo 'selected'?>>Approve</option>
					<option value="Block" <?php if($row['status']=="Block") echo 'selected'?>>Block</option>
				     </select>
				</div>
				</div>
				<div class="ln_solid"></div>
			     <div class="form-group">
				 <div class="col-sm-3 col-md-6 col-sm-6 col-xs-12">
				  <input type="submit" id="Submit" name="Submit" value="Submit" class="btn btn-primary"/>
				  </div>
			     </div>
			   <?php }  ?>
			</form>
			  
			</div>
		</div>
	</div>
</div>
</div>      