<body class="nav-md">
    <!-- page content -->
        <div class="right_col" role="main">
		<div class="row">
		  <div class="col-sm-12 col-md-12 col-xs-12">
		    <div class="x_panel">
			      <div class="x_title">
                    <h2>Change Password</h2>
                    <div class="clearfix"></div>
                  </div>
				   <div class="x_content">
				   <?php
					if($this->session->flashdata('item')){
					$message = $this->session->flashdata('item');
					?>
					<div class="<?php echo $message['class'] ?>"><?php echo $message['message']; ?>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					</div>
					<?php }?>
				    <form method="post" action="<?php echo base_url(); ?>admin/changepassword" class="form-horizontal form-label-left">
					   <div class="form-group">
                        <div class="col-sm-3 col-md-6 col-sm-6 col-xs-12" id="displaynone">
						<input type="email" id="email" name="email" class="form-control" value="<?php if(isset($this->session->user_data['email'])){ echo $this->session->user_data['email'];} ?>" />
						</div>
                      </div>
					   <div class="form-group">
                        <div class="col-sm-3 col-md-6 col-sm-6 col-xs-12">
						<label for="oldpassword">Old password :<span class="text-danger">*</span></label>
						<input type="password" id="oldpassword" name="oldpassword" class="form-control" onBlur="checkoldpassword()" required="required"/>
						<span id="checkoldpassword"></span>
						<span style="color:red"><?php echo form_error('oldpassword'); ?></span>
						</div>
                      </div>
					  <div class="form-group">
                        <div class="col-sm-3 col-md-6 col-sm-6 col-xs-12">
						<label for="newpassword">New password :<span class="text-danger">*</span></label>
						<input type="password" id="newpassword" name="newpassword" class="form-control" required="required"/>
						<span style="color:red"><?php echo form_error('newpassword'); ?></span>
						</div>
                      </div>
					  <div class="form-group">
                        <div class="col-sm-3 col-md-6 col-sm-6 col-xs-12">
						<label for="confirmpassword">Confirm password :<span class="text-danger">*</span></label>
						<input type="password" id="confirmpassword" name="confirmpassword" class="form-control" onchange="checkconfirmpassword()" required="required"/>
						<span id="chkconfirmpwd"></span>
						<span style="color:red"><?php echo form_error('confirmpassword'); ?></span>
						</div>
                      </div>
					  <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-sm-3 col-md-6 col-sm-6 col-xs-12">
						  <input type="submit" name="submit" id="submit" value="Submit" class="btn btn-primary col-xs-12 col-sm-6 col-md-3 "/>
                        </div>
                      </div>
					</form>
				   </div>
			  </div>
		  </div>
		</div>
		</div>
<script>
		function checkconfirmpassword()
		{
			 var newpwd = document.getElementById('newpassword').value;
			 var confirmpwd = document.getElementById('confirmpassword').value;
			if(newpwd != confirmpwd){
				document.getElementById('chkconfirmpwd').style.color = 'red';
                document.getElementById('chkconfirmpwd').innerHTML = 'New Password and Confirm password not match.';
			    document.getElementById('submit').disabled = true;
			}
			else{
				document.getElementById('chkconfirmpwd').innerHTML ="";
				document.getElementById('submit').disabled = false;
			}
		}
		
		function checkoldpassword() {
			var email = $("#email").val();
			var oldpassword = $("#oldpassword").val();
			jQuery.ajax({
			url: "<?php echo base_url(); ?>admin/check_oldpassword_avalibility",
			data: {email : email, password: oldpassword},
			type: "POST",
			success:function(data){
			$("#checkoldpassword").html(data);
			},
			error:function (){}
			});
		}
</script>		