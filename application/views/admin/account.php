<body class="nav-md">
    <!-- page content -->
        <div class="right_col" role="main">
		<div class="row">
		  <div class="col-sm-12 col-md-12 col-xs-12">
		    <div class="x_panel">
			      <div class="x_title">
                    <h2>User Accounts</h2>
                    <div class="clearfix"></div>
                  </div>
				   <div class="x_content">
				   <div class="table-responsive">
			     <table class="table table-hover">
				   <thead  id="trbackground">
                        <tr>
                          <th>SNo</th>
                          <th>Emp ID</th>
                          <th>Name</th>
                          <th>Type</th>
						  <th>Email</th>
						  <th>Status</th>
						  <th>Update</th>
						  <th>Delete</th>
                        </tr>
                      </thead>
					   <tbody>
					   <?php
                             $i = 1; $j = 0;
							 while($i <= $count){
								 $arr[] = $i;
								 $i++;
							 }
						   ?>
						    <?php if($count) {?>
						  <?php foreach($value as $row){?>
						   <tr>
						   <td><?php echo $arr[$j]; ?> </td>
						   <td><?php echo $row['emp_id']; ?> </td>
						   <td><?php echo $row['username']; ?> </td>
						   <td><?php echo $row['type'];?></td>
						   <td><?php echo $row['email']; ?> </td>
						   <td><?php echo $row['status']; ?></td>
						   <td class="text-center">
						   <a href="<?php echo base_url(); ?>admin/user_status?id=<?php echo $row['id']; ?>&email=<?php echo $row['email'];?>"><i class="fa fa-edit text-primary" id="edit-delete-fontsize"></i></a>
						   </td>
							<td class="text-center">
						   <a href="admin-delete-user.php?mail=<?php echo $row['email']?>" onclick="return confirm('Do you want to delete User?');" id="edit-delete-fontsize"><i class="fa fa-trash text-primary"></i></a>
						   </td>
						   </tr>
							<?php $j++; } } else {?>
							<tr class="text-center">
							 <td colspan="8">No Record found.</td>
							 </tr>
							<?php }?>
					   </tbody>
				 </table>
				 </div>
				   </div>
			  </div>
		  </div>
		</div>
		</div>
		