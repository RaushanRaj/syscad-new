<div class="clearfix"></div>
<section id="main">
  <article class="content">
	<div class="container">
	  <div class="row">
	  <div class="title-box">
		<h1 class="title">What We Do</h1>
	  </div>
		<div class="col-sm-6 col-md-6 bottom-padding">
		  <p class="lead">We offer structural steel detailing, fabrication, erection and project management services. We use Tekla software and Autocad to offer fast turnarounds to meet tight schedules. Our core strength is our ability to collaborate very closely with customers and our highly dedicated and motivated employees. We are always ready to take up jobs of any size big or small.</p>
		  <a href="<?php echo base_url(); ?>home/aboutus" class="btn btn-primary">Read more</a>
		</div>
		<div class="col-sm-6 col-md-6 bottom-padding">
		  <div class="progress progress-striped active" data-appear-progress-animation="80%">
			<div class="progress-bar progress-bar-striped active progress-bar-info" style="width: 80%; text-indent: 10px;">3D Modeling using Tekla structures</div>
		  </div>
		  <div class="progress progress-striped active border-radius" data-appear-progress-animation="80%">
			<div class="progress-bar progress-bar-striped active progress-bar-success" style="width: 80%; text-indent: 10px;">Connection design and Checking</div>
		  </div>
		  <div class="progress progress-striped active border-radius" data-appear-progress-animation="80%">
			<div class="progress-bar progress-bar-striped active progress-bar-info" style="width: 80%; text-indent: 10px;">Fabrication Drawings, Erection Drawings & Part Drawings</div>
		  </div>
		  <div class="progress progress-striped active border-radius" data-appear-progress-animation="80%">
			<div class="progress-bar progress-bar-striped active progress-bar-warning" style="width: 80%; text-indent: 10px;">Miscellaneous detailing (stairs, ladders, platforms etc)</div>
		  </div>
		  <div class="progress progress-striped active progress-striped active border-radius" data-appear-progress-animation="80%">
			<div class="progress-bar progress-bar-striped active progress-bar-danger" style="width: 80%; text-indent: 20px;">Production files like NC files, EJE files, KISS file, Fabtrol list,etc</div>
		  </div>
		  <div class="progress progress-striped active border-radius" data-appear-progress-animation="80%">
			<div class="progress-bar progress-bar-striped active progress-bar-success" style="width: 80%; text-indent: 10px;">Material List, Assembly list, Part List, Bolt List, Drawing Index</div>
		  </div>
		</div><!-- .employee -->
	  </div>
	  </div>

	  <div class="bottom-padding"></div>
	<div id="cm-video-bg" class="full-width-box bottom-padding">
	  <div class="band-16">
		<div class="fwb-bg fwb-video band-16">
		<!--.<video autoplay="" muted="" loop="">
		  <source src="<?php echo base_url(); ?>assets/video/Architecture.mp4" type="video/mp4">
		</video>-->
		<div id="overlay"></div>
	  </div>
	  </div>
	  
	  <div class="container">
		<div class="row">
		  <div class="white text-center col-xs-12 col-sm-12 col-md-12">
			<div class="title-box text-center title-white">
			  <h1 class="title">Quality Policy</h1>
			</div>
			<p class="pfontsize">Quality comes first other than anything else. We understand the quality requirements of our Customers/Industry and believe in continual improvement of business process. First time Customers are the Customers forever.</p>
			<a href="<?php echo base_url(); ?>home/solutions" class="btn btn-info">Read more</a>
		  </div>
		</div>
	  </div>
	</div><!-- .full-width-box -->
  <div class="container">
    <div class="row">
    	<div class="title-box">
		<h1 class="title">Portfolio</h1>
	  </div>
      <div class="content gallery col-xs-12 col-sm-12 col-md-12">
		<div class="row">
			<?php foreach($result as $row){?>
			<div class="images-box col-xs-12 col-sm-3 col-md-3 text-center">
			<a class="gallery-images" rel="fancybox" title="<?php echo $row['category']; ?>" href="<?php echo base_url()."".$row['image_path']; ?>">
			  <img src="<?php echo base_url()."".$row['thumbnail']; ?>" width="270" height="197" alt="">
			</a>
			<span class="pfontsize"><?php echo $row['category']; ?></span>
		  </div>
			<?php }  ?>
		</div>
      </div><!-- .content -->
    </div>
  </div><!-- .container -->	
  </article>
</section><!-- #main -->