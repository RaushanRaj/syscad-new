<div class="breadcrumb-box">
  <div class="container">
    <ul class="breadcrumb">
      <li><a href="<?php echo base_url(); ?>">Home</a> </li>
      <li class="active">About Us</li>
    </ul>	
  </div>
</div>
<section id="main">
<header class="page-header">
    <div class="container">
      <h3 class="title">About Us</h3>
    </div>	
</header>
  <article class="content">
	<div class="container">
	<div class="row">
	<div class="content col-sm-12 col-md-12">
		<div class="row">
		  <div class="col-sm-6 col-md-6">
			<p><b>SysCAD Engineering Pvt Ltd.</b> is structural steel & Miscellaneous steel detailing company. It started in 2017 by group of individuals with over 15 years of structural steel detailing, fabrication, erection and project management experience. SysCAD offers variety of structural steel & miscellaneous steel detailing services for the North American construction industry. These projects typically include Schools, Churches, Warehouses, Retail centers, Hospitals, Office Buildings, Power Plants Structures etc. Some of these are very complex, prestigious, fast track and landmark projects. To SysCAD Quality comes prior to profits. We have a work force of 5 detailers, one checker & one Project Manager, we successfully executed steel detailing for the commercial projects and industrial projects. SysCAD uses Tekla software and Autocad to offer fast turnarounds to meet tight schedules. SysCAD core strength is its ability to collaborate very closely with customers and its highly dedicated and motivated employees. SysCAD is always ready to take up jobs of any size big or small.</p>
		  </div>
		  
		  <div class="col-sm-6 col-md-6">
		  <div class="carousel-box overflow no-pagination" data-autoplay-disable="false">
		  <div class="title-box">
			<a class="next" href="#" style="display: inline;">
			  <svg x="0" y="0" width="9px" height="16px" viewBox="0 0 9 16" enable-background="new 0 0 9 16" xml:space="preserve">
				<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#fcfcfc" points="1,0.001 0,1.001 7,8 0,14.999 1,15.999 9,8 "></polygon>
			  </svg>
			</a>
			<a class="prev" href="#" style="display: inline;">
			  <svg x="0" y="0" width="9px" height="16px" viewBox="0 0 9 16" enable-background="new 0 0 9 16" xml:space="preserve">
				<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#fcfcfc" points="8,15.999 9,14.999 2,8 9,1.001 8,0.001 0,8 "></polygon>
			  </svg>
			</a>
			<h2 class="title">Projects</h2>
		  </div>
		  
		  <div class="clearfix"></div>
		  
		  <div class="row">
			<div class="caroufredsel_wrapper" style="text-align: start; float: none; position: relative; top: 0px; right: 0px; bottom: 0px; left: 0px; z-index: auto; width: 1200px; height: 135px; margin: 0px; overflow: hidden;">
			<div class="carousel no-responsive gallery" style="text-align: left; float: none; position: absolute; top: 0px; right: auto; bottom: auto; left: 0px; margin: 0px; width: 4800px; height: 135px;">
			  
			<div class="col-sm-3 col-md-3" style="">
				<a class="gallery-images" rel="fancybox" href="<?php echo base_url(); ?>assets/images/snap-1.png">
				  <img class="replace-2x img-rounded" src="<?php echo base_url(); ?>assets/images/snap-1.png" width="270" height="135" alt="">
				</a>
			  </div>
			  <div class="col-sm-3 col-md-3" style="">
				<a class="gallery-images" rel="fancybox" href="<?php echo base_url(); ?>assets/images/snap-2.png">
				  <img class="replace-2x img-rounded" src="<?php echo base_url(); ?>assets/images/snap-2.png" width="270" height="135" alt="">
				</a>
			  </div>
			  <div class="col-sm-3 col-md-3" style="">
				<a class="gallery-images" rel="fancybox" href="<?php echo base_url(); ?>assets/images/snap-3.png">
				  <img class="replace-2x img-rounded" src="<?php echo base_url(); ?>assets/images/snap-3.png" width="270" height="135" alt="">
				</a>
			  </div>
			  <div class="col-sm-3 col-md-3" style="margin-right: 0px;">
				<a class="gallery-images" rel="fancybox" href="<?php echo base_url(); ?>assets/images/snap-4.png">
				  <img class="replace-2x img-rounded" src="<?php echo base_url(); ?>assets/images/snap-4.png" width="270" height="135" alt="">
				</a>
			  </div>
			  <div class="col-sm-3 col-md-3" style="">
				<a class="gallery-images" rel="fancybox" href="<?php echo base_url(); ?>assets/images/snap-5.png">
				  <img class="replace-2x img-rounded" src="<?php echo base_url(); ?>assets/images/snap-5.png" width="270" height="135" alt="">
				</a>
			  </div>
			  </div>
			  </div>
		  </div>
		</div>
		  </div>
		</div>
		<div class="clearfix"></div>
	  </div>
	</div>
	</div>
  </article>
</section>
<div class="full-width-box bottom-padding cm-padding-bottom-36">
	<div class="fwb-bg fwb-blur" data-blur-image="<?php echo base_url(); ?>assets/images/snap-5.png" data-blur-amount="3"><div class="overlay"></div></div>
	
	<div class="container">
	  <div class="row">
		<div class="col-sm-6 col-md-3 progress-circular bottom-padding bottom-padding-mini" data-appear-animation="fadeInLeftBig">
		  <input type="text" class="knob" value="0" rel="60" data-linecap="round" data-width="200" data-bgcolor="#f2f2f2" data-fgcolor="#738d00" data-thickness=".15" data-readonly="true" disabled="">
		  <p class="white">Commitment</p>
		</div>
		<div class="col-sm-6 col-md-3 progress-circular bottom-padding bottom-padding-mini" data-appear-animation="bounceInLeft">
		  <input type="text" class="knob" value="0" rel="100" data-linecap="round" data-width="200" data-bgcolor="#f2f2f2" data-fgcolor="#c10841" data-thickness=".15" data-readonly="true" disabled="">
		  <p class="white">Honesty</p>
		</div>
		<div class="col-sm-6 col-md-3 progress-circular bottom-padding bottom-padding-mini" data-appear-animation="bounceInRight">
		  <input type="text" class="knob" value="0" rel="50" data-linecap="round" data-width="200" data-bgcolor="#f2f2f2" data-fgcolor="#f89406" data-thickness=".15" data-readonly="true" disabled="">
		  <p class="white">Reliability</p>
		</div>
		<div class="col-sm-6 col-md-3 progress-circular bottom-padding bottom-padding-mini" data-appear-animation="fadeInRightBig">
		  <input type="text" class="knob" value="0" rel="95" data-linecap="round" data-width="200" data-bgcolor="#f2f2f2" data-fgcolor="#0098ca" data-thickness=".15" data-readonly="true" disabled="">
		  <p class="white">Support</p>
		</div>
	  </div>
	  
	  <div class="clearfix"></div>
	</div>
  </div><!-- .full-width-box -->