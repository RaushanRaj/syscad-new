<div class="breadcrumb-box">
  <div class="container">
    <ul class="breadcrumb">
      <li><a href="<?php echo base_url(); ?>">Home</a> </li>
      <li class="active">Gallery</li>
    </ul>	
  </div>
</div>
<section id="main">
<header class="page-header">
    <div class="container">
      <h3 class="title">Gallery</h3>
    </div>	
</header>
  <article class="content">
	<div class="container">
	<div class="content gallery col-sm-12 col-md-12">
	<div class="row">
		<div class="images-box col-sm-6 col-md-6">
				<a class="gallery-images" rel="fancybox" href="<?php echo base_url(); ?>assets/images/Gallery-1.jpeg">
				  <img class="replace-2x" src="<?php echo base_url(); ?>assets/images/Gallery-1.jpeg" width="570" height="416" alt="">
				</a>
		</div>
		<div class="images-box col-sm-6 col-md-6">
				<a class="gallery-images" rel="fancybox" href="<?php echo base_url(); ?>assets/images/Gallery-2.jpeg">
				  <img class="replace-2x" src="<?php echo base_url(); ?>assets/images/Gallery-2.jpeg" width="570" height="416" alt="">
				</a>
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="row">
		<div class="images-box col-sm-6 col-md-6">
				<a class="gallery-images" rel="fancybox" href="<?php echo base_url(); ?>assets/images/Gallery-3.jpeg">
				  <img class="replace-2x" src="<?php echo base_url(); ?>assets/images/Gallery-3.jpeg" width="570" height="416" alt="">
				</a>
		</div>
		<div class="images-box col-sm-6 col-md-6">
				<a class="gallery-images" rel="fancybox" href="<?php echo base_url(); ?>assets/images/Gallery-3.jpeg">
				  <img class="replace-2x" src="<?php echo base_url(); ?>assets/images/Gallery-3.jpeg" width="570" height="416" alt="">
				</a>
		</div>
	</div>
	</div>
	</div>
  </article>
</section>
