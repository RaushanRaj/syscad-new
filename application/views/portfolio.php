<div class="breadcrumb-box">
  <div class="container">
    <ul class="breadcrumb">
      <li><a href="<?php echo base_url(); ?>">Home</a> </li>
      <li class="active">Portfolio</li>
    </ul>	
  </div>
</div>
<section id="main">
<header class="page-header">
    <div class="container">
      <h3 class="title">Portfolio</h3>
    </div>	
</header>
  <article class="content">
	<div class="container">
	<div class="content gallery col-sm-12 col-md-12">
		<div class="row">
			<?php foreach($result as $row){?>
        <div class="images-box col-xm-12 col-sm-12 col-md-4">
				<a class="gallery-images" rel="fancybox" href="<?php echo base_url()."".$row['image_path']; ?>">
				  <img class="replace-2x" src="<?php echo base_url()."".$row['thumbnail']; ?>" alt="">
				</a>
			  </div>
			<?php }  ?>
		</div>
      </div>
	</div>
  </article>
</section>