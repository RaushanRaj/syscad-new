<!doctype html>
<html>
<head>
  <title>SysCAD Engineering Pvt. Ltd.</title>
  <meta class="viewport" name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta charset="utf-8">
  <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/fevicon.png">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/buttons/social-icons.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jslider.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/settings.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.fancybox.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/video-js.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/morris.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/royalslider/royalslider.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/royalslider/skins/minimal-white/rs-minimal-white.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/layerslider/layerslider.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ladda.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/custom.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/responsive.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/customizer/pages.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/customizer/home-pages-customizer.css">

  <!-- IE Styles-->
  <link rel='stylesheet' href="<?php echo base_url(); ?>assets/css/ie/ie.css">
  <style>
  .navbar-nav>li>a{
	  color:#fff;
  }
  .navbar-nav>li>a:hover{
	  background-color:#5DADE2;
	  color:#fff;
	  opacity: 0.35;
  }
  @media (max-width: 767px){.navbar-nav>li>a{text-align: left;display: inline-block;}}
  </style>
</head>
<body class="fixed-header">
<div class="page-box">
<div class="page-box-content">
<header class="header header-two">
	<div class="container-fluid">
	<div class="row" style="background-color:#5DADE2;">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	<section>
	    <div class="container">
            <ul class="nav navbar-nav">
                <li><a href="#"><i class="fa fa-phone"></i> +91 - 40 4855 2500</a></li>
                <li><a href="#"><i class="fa fa-envelope-o"></i> info@syscadengg.com</a></li>
                <li><a href="#"><i class="fa fa-clock-o"></i> Mon - Fri 9AM – 6PM</a></li>
            </ul>
            <ul class="nav navbar-nav pull-right socailicondisable">
                <li><a href="https://www.facebook.com/Syscad-Engineering-439083513151965/"  class="col-xs-12 col-sm-12"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="https://twitter.com/syscadengg"  class="col-xs-12 col-sm-12"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
				<li><a href="https://www.linkedin.com/company/syscad-engineering/"  class="col-xs-12 col-sm-12"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
            </ul>
	    </div>
	</section>
	</div>
	</div>
	</div>
  <div class="container">
	<div class="row">
	  <div class="col-xs-6 col-md-2 col-lg-3 logo-box">
		<div class="logo">
		  <a href="<?php echo base_url(); ?>">
			<img src="<?php echo base_url(); ?>assets/logo/logo.jpg" class="logo-img" alt="">
		  </a>
		</div>
	  </div><!-- .logo-box -->
	  
	  <div class="col-xs-6 col-md-10 col-lg-9 right-box">
		<div class="right-box-wrapper">
		  <div class="primary">
			<div class="navbar navbar-default" role="navigation">
			  <button type="button" class="navbar-toggle btn-navbar collapsed" data-toggle="collapse" data-target=".primary .navbar-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
	
			  <nav class="collapse collapsing navbar-collapse">
				  <ul class="nav navbar-nav navbar-center">
					<li>
					  <a href="<?php echo base_url(); ?>">Home</a>
					</li>
					<li class="parent">
					  <a href="<?php echo base_url(); ?>home/services">Services</a>
					  <ul class="sub">
					  <li><a href="<?php echo base_url(); ?>home/solutions">Solutions</a></li>
					  </ul>
					</li>
					<li>
					  <a href="<?php echo base_url(); ?>home/quality">Quality</a>
					</li>
					<li>
					  <a href="<?php echo base_url(); ?>home/portfolio">Portfolio</a>
					</li>
					<li>
					  <a href="<?php echo base_url(); ?>home/project">Projects</a>
					</li>
					<li class="parent">
					  <a href="#">Company</a>
					  <ul class="sub">
					  <li><a href="<?php echo base_url(); ?>home/aboutus">About Us</a></li>
					  <li><a href="<?php echo base_url(); ?>home/ourteam">Our Team</a></li>
					  <li><a href="<?php echo base_url(); ?>home/gallery">Gallery</a></li>
					  </ul>
					</li>
					<li>
					  <a href="<?php echo base_url(); ?>home/contactus">Contact Us</a>
					</li>
				  </ul>
				</nav>
			</div>
		  </div><!-- .primary -->
		</div>
	  </div>
	</div><!--.row -->
  </div>
</header><!-- .header -->