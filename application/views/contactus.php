<div class="breadcrumb-box">
  <div class="container">
    <ul class="breadcrumb">
      <li><a href="<?php echo base_url(); ?>">Home</a> </li>
      <li class="active">Contact Us</li>
    </ul>	
  </div>
</div>
<section id="main">
<header class="page-header">
    <div class="container">
      <h3 class="title">Contact Us</h3>
    </div>	
</header>
<article class="content">
	<div class="container">
		<div class="row">
		<div class="content col-xm-12 col-sm-12 col-md-12 bottom-padding">
		   <iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?q=SysCAD+Engineering+Pvt+Ltd.&amp;t=m&amp;z=10&amp;output=embed&amp;iwloc=near"width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>
		</div>
		
		<div class="row">
		 <div class="content col-sm-12 col-md-12">
		<div class="row">
		  <div class="col-sm-6 col-md-6 bottom-padding">
			<address>
			  <h3 class="title-box">Main Office</h3>
			  <div><b>SysCAD Engineering Private Limited.</b></div>
			  <br/>
			  <div>8-2-618/2/DC/B/102., B-Block, First Floor,</div>
			  <div>Delta Seacon Complex, Road No:11,</div>
			  <div>Banjara Hills, Hyderabad-500034, INDIA.</div>
			   <br/>
			  <div><b>Tel :</b> +91 - 40 4855 2500</div>
			  <div><b>Email :</b> <a href="mailto:support@example.com" target="_blank">info@syscadengg.com</a></div>
			</address>
		  </div>
		  <div class="col-sm-6 col-md-6 bottom-padding">
			<form id="contactform" action="<?php echo base_url(); ?>home/contactmail" class="form-box register-form contact-form" method="POST">
			 <?php
					if($this->session->flashdata('item')){
					$message = $this->session->flashdata('item');
					?>
					<div class="<?php echo $message['class'] ?>"><?php echo $message['message']; ?>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
					</button>
					</div>
					<?php }?>
			  <h3 class="title">Quick Contact</h3>
			  <div id="success"></div>
			  <div class="form-group">
			  <label>Name: <span class="required">*</span></label>
			  <input class="form-control" type="text" name="name" autofocus required>
			  <span style="color:red"><?php echo form_error('name'); ?></span>
              </div>
              <div class="form-group">
			  <label>Email Address: <span class="required">*</span></label>
			  <input class="form-control" type="email" name="email" required>
			  <span style="color:red"><?php echo form_error('email'); ?></span>
			   </div>
			   <div class="form-group">
			  <label>Telephone:</label>
			  <input class="form-control" type="text" name="phone" maxlength="10" required>
			  <span style="color:red"><?php echo form_error('phone'); ?></span>
			   </div>
			   <div class="form-group">
			  <label>Comment:</label>
			  <textarea class="form-control" name="comment" required></textarea>
			  <span style="color:red"><?php echo form_error('comment'); ?></span>
			   </div>
			  <div class="clearfix"></div>
			  <div class="buttons-box clearfix">
			  <input type="submit" name="submit" value="Submit" class="btn btn-primary">
				
				<span class="required"><b>*</b> Required Field</span>
			  </div><!-- .buttons-box -->
			</form>
		  </div>
      </div>
		</div>
	</div>
</article>
</section>