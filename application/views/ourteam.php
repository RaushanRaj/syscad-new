<div class="breadcrumb-box">
  <div class="container">
    <ul class="breadcrumb">
      <li><a href="<?php echo base_url(); ?>">Home</a> </li>
      <li class="active">Our Team</li>
    </ul>	
  </div>
</div>
<section id="main">
<header class="page-header">
    <div class="container">
      <h3 class="title">Our Team</h3>
    </div>
	
</header>
  <article class="content">
	<div class="container">
     <div class="row">
	 <div class="content col-sm-12 col-md-12">
	 <p>We have excellent work force of detailers, editors, checkers & Project Managers, we successfully executed steel detailing for the commercial projects and industrial projects. SysCAD uses Tekla software and Autocad to offer fast turnarounds to meet tight schedules. SysCAD core strength is its ability to collaborate very closely with customers and its highly dedicated and motivated employees. SysCAD is always ready to take up jobs of small or big size.</p>
	 </div>
	 </div>
	 <div class="row">
	 <div class="content col-sm-12 col-md-12">
	 <div class="title-box">
			<h2 class="title">Strengths</h2>
		  </div>
		   <p>SysCAD core strength is STEEL DETAILING. SysCAD Top management comprises of Professionals with over Decade’s Experience of  expertise in detailing, fabrication, erection and project management of structural steel projects , which enables the projects being delivered to meet the quality requirement of the industry, very fast turnarounds that the industry needs today.</p>
	 </div>
	 </div>
	</div>
  </article>
</section>