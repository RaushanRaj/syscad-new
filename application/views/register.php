<div class="breadcrumb-box">
  <div class="container">
   <ul class="breadcrumb">
      <li><a href="<?php echo base_url(); ?>">Home</a> </li>
      <li class="active">Register</li>
    </ul>	
  </div>
</div><!-- .breadcrumb-box -->
<section id="main" class="login-register">
  <header class="">
    <div class="container">
      <h3 class="title"></h3>
    </div>	
  </header>
  <div class="container">
    <div class="row">
      <div class="content col-sm-12 col-md-12">
		  <div class="col-xs-12 col-sm-6 col-md-6 box register">
		  <?php
             if($id['id'] == "")
			   {
				  $cust = "UID00";
				  $cust1 = "1";
				  $cust_id = $cust."".$cust1;
			   }
			  else
			  {
				  $cust = "UID00";
				  $n = "1";
				  $cust2 = ($id['id'] + $n);
				  $cust_id = $cust."".$cust2;
			  } 
			  
		  ?>
		  <?php/*  echo validation_errors();  */?>
			<form class="form-box register-form" method="post" action="<?php echo base_url(); ?>home/registeruser">
			 <?php
			if($this->session->flashdata('item'))
			{
			$message = $this->session->flashdata('item');
			?>
		<div class="<?php echo $message['class'] ?>"><?php echo $message['message']; ?>
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	    </div>
			<?php }?>
			  <h3 class="title">Registered customers</h3>
			  <p>If you don't have an account with us, please register.</p>
			  
			  <div class="form-group">
			  <label>Employee ID: <span class="required">*</span></label>
			  <input type="text" class="form-control" name="employeeId" value="<?php if(isset($cust_id)){ echo $cust_id; } ?>" readonly>
			  </div>
			  
			  <div class="form-group">
			  <label>Username: <span class="required">*</span></label>
			  <span style="color:red"><?php echo form_error('username'); ?></span>
			  <input type="text" class="form-control" name="username" placeholder="Username" required="required">
			 
			  </div>
				<div class="form-group">
			  <label>Email: <span class="required">*</span></label>
			  <span style="color:red"><?php echo form_error('email'); ?></span> 
			  <input type="email" class="form-control" id="email" name="email" placeholder="Email" required="required">
			 
			 </div>
			  <div class="form-group">
			  <label>Password: <span class="required">*</span></label>
			  <span style="color:red"><?php echo form_error('password'); ?></span>
			  <input type="password" class="form-control" name="password" placeholder="*******" required="required">
			  </div>
				<div class="form-group">
			  <label>Security Text: <span class="required">*</span> </label>
			    <span style="color:red"><?php echo form_error('securityText'); ?></span>
			  <input type="text" class="form-control" name="securityText" placeholder="Security Text" required="required">
			 
			  </div>
			  <div class="buttons-box clearfix">
				<input type="submit" class="btn btn-default col-xs-12 col-sm-6 col-md-3" name="register" value="Register">
				<span class="required pull-right"><b>*</b> Required Field</span>
			  </div>
			</form>
		  </div>

		  <div class="col-xs-12 col-sm-6 col-md-6 box register">
			<div class="info-box">
			  <h3 class="title">My Account</h3>
			 <!--. <p class="descriptions"></p>-->
			   <a href="<?php echo base_url(); ?>home/login" class="btn btn-default">Login an Account</a>
			</div>
		  </div>
      </div>
    </div>
  </div><!-- .container -->
</section><!-- #main -->