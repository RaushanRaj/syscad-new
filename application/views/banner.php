<div class="slider rs-slider full-width load">
    <div class="tp-banner-container">
    <div class="rev_slider tp-banner">
      <ul>
      <li data-delay="7000" data-transition="slidehorizontal" data-masterspeed="1000">
        <div class="elements">
        <h2 class="tp-caption title"
          data-x="15"
          data-y="240"
          data-start="700"
          data-transform_in="x:left;s:1000;e:Power4.easeOut"
          data-transform_out="x:left;s:400;e:Power1.easeIn">
        </h2>
  
        <a href="#"
          class="tp-caption btn btn-link"
          data-x="15"
          data-y="500"
          data-start="700"
          data-transform_in="y:bottom;s:1000;e:Power4.easeOut"
          data-transform_out="y:bottom;s:400;e:Power1.easeIn">
          View Our Portfolio<i class="fa fa-angle-right"></i></a>
        </a>
        </div>
        <img class="replace-2x" src="<?php echo base_url(); ?>assets/images/snap-5.png" alt="" data-bgfit="cover" data-bgposition="center top" data-bgrepeat="no-repeat">
      </li>
      
      <li data-delay="7000" data-transition="slidehorizontal" data-masterspeed="1000">
        <div class="elements">
        <h2 class="tp-caption title text-center"
          data-x="center"
          data-y="268"
          data-start="700"
          data-transform_in="y:top;s:1000;e:Power4.easeOut"
          data-transform_out="y:top;s:400;e:Power1.easeIn">
          View Our Portfolio
        </h2>
        </a>
        </div>
        
        <img class="replace-2x" src="<?php echo base_url(); ?>assets/images/snap_009.png" alt="" data-bgfit="cover" data-bgposition="center top" data-bgrepeat="no-repeat">
      </li>
      
      <li data-delay="7000" data-transition="slidehorizontal" data-masterspeed="1000">
        <div class="elements right-box">
        <h2 class="tp-caption title text-right"
          data-x="right"
          data-hoffset="15"
          data-y="350"
          data-start="700"
          data-transform_in="x:right;s:1000;e:Power4.easeOut"
          data-transform_out="x:right;s:400;e:Power1.easeIn">
        </h2>
  
        <a href="#"
          class="tp-caption btn btn-link"
          data-x="right"
          data-hoffset="15"
          data-y="480"
          data-start="700"
          data-transform_in="y:bottom;s:1000;e:Power4.easeOut"
          data-transform_out="y:bottom;s:400;e:Power1.easeIn">
            <i class="fa fa-angle-right"></i></a>
        </a>
        </div>
        
        <img class="replace-2x" src="<?php echo base_url(); ?>assets/images/snap_006-1.png" alt="" data-bgfit="cover" data-bgposition="center top" data-bgrepeat="no-repeat">
      </li>
      </ul>
    </div><!-- .tp-banner -->
    </div>
  </div><!-- .rs-slider -->