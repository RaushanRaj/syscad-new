<div class="breadcrumb-box">
  <div class="container">
    <ul class="breadcrumb">
      <li><a href="<?php echo base_url(); ?>">Home</a> </li>
      <li class="active">Projects</li>
    </ul>	
  </div>
</div>
<section id="main">
<header class="page-header">
    <div class="container">
      <h3 class="title">Projects Executed</h3>
    </div>	
</header>
  <article class="content">
	<div class="container">
	<div class="row">
	<div class="content-block bottom-padding frame-shadow-raised col-xm-12 col-sm-6 col-md-5">
		  <div class="col-sm-6 col-md-4">
			  <div>Project Ref</div>
			  <div>Project Name</div>
			  <div>Location</div>
			  <div>Contractor Name</div>
		  </div>
		  <div class="col-sm-6 col-md-8">
			  <div>8426</div>
			  <div>UTMB - Health Education Center</div>
			  <div>GALVESTON , TX.</div>
			  <div>VAUGHN CONSTRUCTION</div>
		  </div>
	  </div>
	  <div class="col-xm-12 col-sm-6 col-md-2"></div>
	  <div class="content-block bottom-padding frame-shadow-raised col-xm-12 col-sm-6 col-md-5">
	        <div class="col-sm-6 col-md-4">
			  <div>Project Ref</div>
			  <div>Project Name</div>
			  <div>Location</div>
			  <div>Contractor Name</div>
		  </div>
		  <div class="col-sm-6 col-md-8">
			  <div>8416</div>
			  <div>Eagle Traces R B 2.2</div>
			  <div>Houston, TX</div>
			  <div>WHITING & TUNER</div>
		  </div>
	  </div>
	</div>
	 <div class="row">
	<div class="content-block bottom-padding frame-shadow-raised col-xm-12 col-sm-6 col-md-5">
		 <div class="col-sm-6 col-md-4">
			  <div>Project Ref</div>
			  <div>Project Name</div>
			  <div>Location</div>
			  <div>Contractor Name</div>
		  </div>
		  <div class="col-sm-6 col-md-8">
			  <div>8414</div>
			  <div>TAMU SYSTEM BUILDING</div>
			  <div>COLLEGE STATION, TX</div>
			  <div>RECREATION INDUSTRIES</div>
		  </div>
	  </div>
	  <div class="col-xm-12 col-sm-6 col-md-2"></div>
	  <div class="content-block bottom-padding frame-shadow-raised col-xm-12 col-sm-6 col-md-5">
	        <div class="col-sm-6 col-md-4">
			  <div>Project Ref</div>
			  <div>Project Name</div>
			  <div>Location</div>
			  <div>Contractor Name</div>
		  </div>
		  <div class="col-sm-6 col-md-8">
			  <div>8396</div>
			  <div>El Franco Lee Tower</div>
			  <div>HOUSTON, TX</div>
			  <div>Horizon Group International</div>
		  </div>
	  </div>
	</div>
	
	<div class="row">
	<div class="content-block bottom-padding frame-shadow-raised col-xm-12 col-sm-6 col-md-5">
		  <div class="col-sm-6 col-md-4">
			  <div>Project Ref</div>
			  <div>Project Name</div>
			  <div>Location</div>
			  <div>Contractor Name</div>
		  </div>
		  <div class="col-sm-6 col-md-8">
			  <div>8393</div>
			  <div>MDACC Anderson Refeed</div>
			  <div>HOUSTON, TX</div>
			  <div>CACTUS BUILDERS</div>
		  </div>
	  </div>
	  <div class="col-xm-12 col-sm-6 col-md-2"></div>
	  <div class="content-block bottom-padding frame-shadow-raised col-xm-12 col-sm-6 col-md-5">
	        <div class="col-sm-6 col-md-4">
			  <div>Project Ref</div>
			  <div>Project Name</div>
			  <div>Location</div>
			  <div>Contractor Name</div>
		  </div>
		  <div class="col-sm-6 col-md-8">
			  <div>8390</div>
			  <div>MDACC PB.3115 Wall plates</div>
			  <div>HOUSTON, TX</div>
			  <div>CACTUS BUILDERS</div>
		  </div>
	  </div>
	</div>
	
	<div class="row">
	<div class="content-block bottom-padding frame-shadow-raised col-xm-12 col-sm-6 col-md-5">
		  <div class="col-sm-6 col-md-4">
			  <div>Project Ref</div>
			  <div>Project Name</div>
			  <div>Location</div>
			  <div>Contractor Name</div>
		  </div>
		  <div class="col-sm-6 col-md-8">
			  <div>8373</div>
			  <div>WILLOWBROOK MEDICAL CENTER</div>
			  <div>HOUSTON, TX</div>
			  <div>HDAI CONSTRUCTION</div>
		  </div>
	  </div>
	  <div class="col-xm-12 col-sm-6 col-md-2"></div>
	  <div class="content-block bottom-padding frame-shadow-raised col-xm-12 col-sm-6 col-md-5">
	        <div class="col-sm-6 col-md-4">
			  <div>Project Ref</div>
			  <div>Project Name</div>
			  <div>Location</div>
			  <div>Contractor Name</div>
		  </div>
		  <div class="col-sm-6 col-md-8">
			  <div>8364</div>
			  <div>Aspen Heights-MLK Garage</div>
			  <div>Houston, Texas</div>
			  <div>TELLEPSEN BUILDERS</div>
		  </div>
	  </div>
	</div>
	
	<div class="row">
	<div class="content-block bottom-padding frame-shadow-raised col-xm-12 col-sm-6 col-md-5">
		  <div class="col-sm-6 col-md-4">
			  <div>Project Ref</div>
			  <div>Project Name</div>
			  <div>Location</div>
			  <div>Contractor Name</div>
		  </div>
		  <div class="col-sm-6 col-md-8">
			  <div>8363</div>
			  <div>LOCAL FOODS #4</div>
			  <div>120 N MAIN ST./PASAD</div>
			  <div></div>
		  </div>
	  </div>
	  <div class="col-xm-12 col-sm-6 col-md-2"></div>
	  <div class="content-block bottom-padding frame-shadow-raised col-xm-12 col-sm-6 col-md-5">
	        <div class="col-sm-6 col-md-4">
			  <div>Project Ref</div>
			  <div>Project Name</div>
			  <div>Location</div>
			  <div>Contractor Name</div>
		  </div>
		  <div class="col-sm-6 col-md-8">
			  <div>8361</div>
			  <div>Bellaire Nature Discovery Center</div>
			  <div>Bellaire, TX</div>
			  <div>HORIZON INTERNATIONAL GROUP</div>
		  </div>
	  </div>
	</div>
	
	</div>
	</div>
  </article>
</section>