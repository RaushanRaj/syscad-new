<div class="breadcrumb-box">
  <div class="container">
    <ul class="breadcrumb">
      <li><a href="<?php echo base_url(); ?>">Home</a></li>
      <li class="active">Services</li>
    </ul>	
  </div>
</div>
<section id="main">
<header class="page-header">
    <div class="container">
      <h3 class="title">Services</h3>
    </div>	
</header>
  <article class="content">
	<div class="container">
	<div class="content gallery col-xs-12 col-sm-12 col-md-12">
	<div class="row">
		<div class="images-box col-sm-6 col-md-6">
			<a class="gallery-images" rel="fancybox" href="<?php echo base_url(); ?>assets/services/3D_Modelling1.png">
				  <img class="replace-2x" src="<?php echo base_url(); ?>assets/services/3D_Modelling.png" width="570" height="416" alt="">
		    </a>
		</div>
		<div class="images-box col-sm-6 col-md-6">
				<h3>3D Modelling</h3>
				<p>We offer 3D modeling covering almost all types of constructions using TEKLA software to ensure quality and accuracy to our clients. Using the 2D drawings we develop 3D models and coordinate with other trades to avoid uninvited problems at the job site.</p>
		        <p>3D modeling provides you a better understanding on the project even before you start your real work. We offer 3D modeling for almost all types of buildings in the construction industry using TEKLA structures software to ensure quality and accuracy. We are never behind our client’s expectations when it comes to coordinating with the other trades and providing clear flawless drawings as the output. Using the 2D design drawings we receive, we build the 3D model and check for the possibility of erection and fabrication.</p>
		</div>  
	</div>
	<div class="clearfix"></div>
	<div class="row">
		<div class="images-box col-sm-6 col-md-6">
			<h3>BIM Coordination</h3>
				<p>The solution for tight budgets, limited man power, aggressive schedules is here with us and the world calls it Building Information Modelling (BIM). BIM is the best approach to coordinate with all the other trades and investigate the interference of one object of one trade into another object of other trade. This avoids the unwanted surprises in the field and saves money, time and material.</p>
		        <p>We follow the best ways in the domain to coordinate with other trades as well. We have good number of successful jobs under our name dealing with BIM coordination.</p>
		</div>
		<div class="images-box col-sm-6 col-md-6">
			<a class="gallery-images" rel="fancybox" href="<?php echo base_url(); ?>assets/services/BIM_Coordination.png">
				  <img class="replace-2x" src="<?php echo base_url(); ?>assets/services/BIM_Coordination.png" width="570" height="416" alt="">
		    </a>
		</div>  
	</div>
	<div class="clearfix"></div>
	<div class="row">
		<div class="images-box col-sm-6 col-md-6">
			<a class="gallery-images" rel="fancybox" href="<?php echo base_url(); ?>assets/services/Industrial_jobs_modelling1.png">
				  <img class="replace-2x" src="<?php echo base_url(); ?>assets/services/Industrial_jobs_modelling.png" width="570" height="416" alt="">
		    </a>
		</div>
		<div class="images-box col-sm-6 col-md-6">
			<h3>Offshore and Industrial jobs modelling</h3>
			<p>In general, offshore jobs prefer high accuracy. And if you are here in search of it, you are at the right spot!</p>
	        <p>SysCAD supports its customers in 3D modelling and detailing of Offshore jobs. We model the super structure of the project using the design drawings to locate the primary/secondary steel, handrails, ladders and pipes making sure that everything is in line with the requirements. We release shop drawings and erection drawings with 1/16” accuracy after coordinating with the clients and getting approvals.</p>
		</div>  
	</div>
	<div class="clearfix"></div>
	<div class="row">
		<div class="images-box col-sm-6 col-md-6">
			<h3>Steel Detailing</h3>
			<p>SysCAD is into Steel detailing since its birth. We have well trained technical team that is involved in Steel detailing. We use TEKLA structures detailing software to help our customers in all possible ways. Our deliverables include Anchor bolt plans, Embed plans, Shop drawings, Erection plans, sections and details, NC files, DXF files, EJE / MIS list, Field bolt lists and other reports. We also provide advance bill of materials to help our customers order the material prior to fabrication.</p>
	        <p>We will know the specifications and other requirements of the customers and customize our drawings accordingly. A pre-detailing meeting at the beginning of the project and status meetings now and then helps us to reach our customers expectations. </p>
		</div>
		<div class="images-box col-sm-6 col-md-6">
			<a class="gallery-images" rel="fancybox" href="<?php echo base_url(); ?>assets/services/Steel_Detailing1.png">
				  <img class="replace-2x" src="<?php echo base_url(); ?>assets/services/Steel_Detailing.png" width="570" height="416" alt="">
		    </a>
		</div>  
	</div>
	</div>
	</div>
  </article>
</section>
