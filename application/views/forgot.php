<div class="breadcrumb-box">
  <div class="container">
    <ul class="breadcrumb">
      <li><a href="<?php echo base_url(); ?>">Home</a> </li>
      <li class="active">Login</li>
    </ul>	
  </div>
</div><!-- .breadcrumb-box -->

<section id="main" class="login-register">
  <header class="">
    <div class="container">
      <h3 class="title"></h3>
    </div>	
  </header>
  <div class="container">
    <div class="row">
      <div class="content col-sm-12 col-md-12">
		<div class="row">
		  <div class="col-xs-12 col-sm-6 col-md-6">
			<form class="form-box login-form" method="post" action="">
			  <h3 class="title">Retrieve your password here</h3>
			  <p>Please enter your email address below. You will receive a link to reset your password.</p>
			  <div>
			  <label>Email Address: <span class="required">*</span></label>
			  <span id="emailavailability"></span>
			  <input class="form-control" type="email" id="email" name="email" placeholder="Email Address" required="required" autofocus onBlur="checkAvailability()">
			  </div>
			  <div>
			  <label>Security Text: <span class="required">*</span></label>
			  <input type="text" class="form-control" name="security_txt" placeholder="Security Text" required="required">
              </div>
			  <div class="buttons-box clearfix">
				<input type="submit" class="btn btn-default" name="forgot" value="Forgot Password">
				<span class="required pull-right"><b>*</b> Required Field</span>
			  </div>
			</form><!-- .form-box -->
		  </div>
		  
		  <div class="col-xs-12 col-sm-6 col-md-6">
			<div class="info-box">
			  <h3 class="title">Password recovery</h3>
			  <!--.<p class="descriptions"></p>-->
			  <a href="<?php echo base_url(); ?>home/login" class="btn">Back to Login</a>
			</div>
		  </div>
		</div>
      </div>
    </div>
  </div><!-- .container -->
</section><!-- #main -->