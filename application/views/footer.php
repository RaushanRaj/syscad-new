
</div><!-- .page-box-content -->
</div><!-- .page-box -->
<footer id="footer">
  <div class="footer-top">
    <div class="container">
      <div class="row sidebar">
	  <aside class="col-xs-12 col-sm-6 col-md-6 widget links">
		  <div class="title-block">
			<h3 class="title">Contact Info</h3>
		  </div>
		  <nav>
			<ul>
			  <li class="col-xs-12">
			  <div class="phone">
			  <div class="footer-icon">
				<svg x="0" y="0" width="16px" height="16px" viewBox="0 0 16 16" enable-background="new 0 0 16 16" xml:space="preserve">
				  <path fill="#c6c6c6" d="M11.001,0H5C3.896,0,3,0.896,3,2c0,0.273,0,11.727,0,12c0,1.104,0.896,2,2,2h6c1.104,0,2-0.896,2-2
				  c0-0.273,0-11.727,0-12C13.001,0.896,12.105,0,11.001,0z M8,15c-0.552,0-1-0.447-1-1s0.448-1,1-1s1,0.447,1,1S8.553,15,8,15z
				  M11.001,12H5V2h6V12z"></path>
				</svg>
			  </div>
			  <strong class="title lifontsize">Call Us:</strong><span class="lifontsize"> +91 - 40 4855 2500</span> <br>
			</div>
			</li>
			<li class="col-xs-12 lifontsize">
			  <div class="phone">
			  <div class="footer-icon">
                 
			  </div>
			  <strong class="title">Email:</strong> info@syscadengg.com <br>
			  </div>
			  </li>
			  <li class="col-xs-12 lifontsize">
			  <div class="address">
          <div class="footer-icon">
			<svg x="0" y="0" width="16px" height="16px" viewBox="0 0 16 16" enable-background="new 0 0 16 16" xml:space="preserve">
			  <g>
				<g>
				  <path fill="#c6c6c6" d="M8,16c-0.256,0-0.512-0.098-0.707-0.293C7.077,15.491,2,10.364,2,6c0-3.309,2.691-6,6-6
				  c3.309,0,6,2.691,6,6c0,4.364-5.077,9.491-5.293,9.707C8.512,15.902,8.256,16,8,16z M8,2C5.795,2,4,3.794,4,6
				  c0,2.496,2.459,5.799,4,7.536c1.541-1.737,4-5.04,4-7.536C12.001,3.794,10.206,2,8,2z"></path>
				</g>
				<g>
				  <circle fill="#c6c6c6" cx="8.001" cy="6" r="2"></circle>
				</g>
			  </g>
			</svg>
		  </div>
        <strong class="title lifontsize">Address:</strong> 8-2-618/2/DC/B/102. ,B-Block, First Floor,<br>
Delta Seacon Complex, Road No:11,<br>
Banjara Hills, Hyderabad-500034, INDIA. <br>
        </div>
			  </li>
			</ul>
		  </nav>
		</aside>
		
		<aside class="col-xs-12 col-sm-6 col-md-3 widget social">
		  <div class="title-block">
			<h3 class="title">Follow Us</h3>
		  </div>
          <p>Follow us in social media</p>
          <a class="sbtnf sbtnf-rounded color color-hover icon-facebook" href="https://www.facebook.com/Syscad-Engineering-439083513151965/"></a>
          <a class="sbtnf sbtnf-rounded color color-hover icon-twitter" href="https://twitter.com/syscadengg"></a>
          <a class="sbtnf sbtnf-rounded color color-hover icon-linkedin" href="https://www.linkedin.com/company/syscad-engineering/"></a>
		  <div class="clearfix"></div>
        </aside>
	  </div>
	</div>
  </div><!-- .footer-top -->
	
  <div class="footer-bottom">
    <div class="container">
      <div class="row">
        <div class="copyright col-xs-12 col-sm-9 col-md-9 lifontsize">
		Copyright © 2017 SysCAD Engineering Pvt Ltd, All Rights Reserved.
		</div>
	
        <div class="col-xs-12 col-sm-3 col-md-3">
          <a href="#" class="up" title="Top">
			<span class="glyphicon glyphicon-arrow-up"></span>
		  </a>
        </div>
      </div>
    </div>
  </div><!-- .footer-bottom -->
</footer>
<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/price-regulator/jshashtable-2.1_src.js"></script>
<script src="<?php echo base_url(); ?>assets/js/price-regulator/jquery.numberformatter-1.2.3.js"></script>
<script src="<?php echo base_url(); ?>assets/js/price-regulator/tmpl.js"></script>
<script src="<?php echo base_url(); ?>assets/js/price-regulator/jquery.dependClass-0.1.js"></script>
<script src="<?php echo base_url(); ?>assets/js/price-regulator/draggable-0.1.js"></script>
<script src="<?php echo base_url(); ?>assets/js/price-regulator/jquery.slider.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.carouFredSel-6.2.1-packed.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.touchSwipe.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.elevateZoom-3.0.8.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.imagesloaded.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.appear.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.easypiechart.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.easing.1.3.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.fancybox.pack.js"></script>
<script src="<?php echo base_url(); ?>assets/js/isotope.pkgd.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.knob.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.stellar.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.selectBox.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.royalslider.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.tubular.1.0.js"></script>
<script src="<?php echo base_url(); ?>assets/js/country.js"></script>
<script src="<?php echo base_url(); ?>assets/js/spin.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/ladda.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/masonry.pkgd.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/morris.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/raphael.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/video.js"></script>
<script src="<?php echo base_url(); ?>assets/js/pixastic.custom.js"></script>
<script src="<?php echo base_url(); ?>assets/js/livicons-1.3.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/layerslider/greensock.js"></script>
<script src="<?php echo base_url(); ?>assets/js/layerslider/layerslider.transitions.js"></script>
<script src="<?php echo base_url(); ?>assets/js/layerslider/layerslider.kreaturamedia.jquery.js"></script>
<script src="<?php echo base_url(); ?>assets/js/revolution/jquery.themepunch.plugins.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/revolution/jquery.themepunch.revolution.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/main.js"></script>

</body>
</html>