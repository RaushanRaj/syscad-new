<div class="breadcrumb-box">
  <div class="container">
    <ul class="breadcrumb">
      <li><a href="<?php echo base_url(); ?>home/index">Home</a> </li>
      <li class="active">Login</li>
    </ul>	
  </div>
</div><!-- .breadcrumb-box -->
<section id="main" class="login-register">
  <header class="">
    <div class="container">
      <h3 class="title"></h3>
    </div>	
  </header>
  <div class="container">
    <div class="row">
      <div class="content col-sm-12 col-md-12">
		  <div class="col-xs-12 col-sm-6 col-md-6 box login">
			<div class="info-box">
			  <h3 class="title">New customers</h3>
			  <a href="<?php echo base_url(); ?>home/register" class="btn btn-default">Create an Account</a>
			</div>
		  </div>

		  <div class="col-xs-12 col-sm-6 col-md-6 box login">
			<form class="form-box login-form" method="post" action="<?php echo base_url(); ?>home/validate_login">
			<?php
			if($this->session->flashdata('item'))
			{
			$message = $this->session->flashdata('item');
			?>
			<div class="<?php echo $message['class'] ?>"><?php echo $message['message']; ?>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			</div>
			<?php }?>
			  <h3 class="title">Login customers</h3>
			  <p>If you have an account with us, please log in.</p>
			  <div class="form-group">
			  <label>Email Address: <span class="required">*</span></label>
			  <span style="color:red"><?php echo form_error('email'); ?></span>
			  <input type="email" id="email" class="form-control" name="email" placeholder="Email Address"  autofocus >
			  </div>
			  <div class="form-group">
			  <label>Password: <span class="required">*</span></label>
			  <span style="color:red"><?php echo form_error('password'); ?></span>
			  <input  type="password" id="password" class="form-control" name="password" placeholder="******" >
			  </div>
			  <!-- .<div class="checkbox">
				<label class=""><input type="checkbox"> Remember password</label>
			  </div>-->
			  
			  <div class="buttons-box clearfix">
				<input type="submit" class="btn btn-default col-xs-12 col-sm-6 col-md-3" name="login" value="Login">
				<div class="pull-right">
				<a href="<?php echo base_url(); ?>home/forgot" class="forgot">Forgot Your Password?</a>
				<span class="required"><b>*</b> Required Field</span>
				</div>
			  </div>
			</form><!-- .form-box -->
		  </div>
      </div>
    </div>
  </div><!-- .container -->
</section><!-- #main -->