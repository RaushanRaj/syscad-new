<div class="breadcrumb-box">
  <div class="container">
    <ul class="breadcrumb">
      <li><a href="<?php echo base_url(); ?>">Home</a> </li>
      <li class="active">Quality Policy</li>
    </ul>	
  </div>
</div>
<section id="main">
  <article class="content">
	<div class="container">
	  <div>
		<div>
		  <div class="title-box" data-appear-animation="fadeInDown">
			<h3 class="title">Quality Policy</h3>
		  </div>
		  <p data-appear-animation="bounce" class="text-center">Quality comes first for SysCAD other than anything else. SysCAD understands the quality requirement of Customer/Industry and believes in continual improvement of business process to achieve the highest quality standards. First time Customers are the Customers forever.</p>
		</div>
		<div class="clearfix"></div>
	  </div>
	  
		<div class="title-box" data-appear-animation="fadeInDown">
		  <h1 class="title">Services</h1>
		</div>
		<div class="row services">
		  <div class="service col-sm-4 col-md-4" data-appear-animation="bounceInLeft">
			<a href="#">
			  <div class="icon border border-error">
				<div class="livicon" data-n="tablet" data-s="42" data-c="#c10841" data-hc="0"></div>
			  </div>
			  <h6 class="title">Structural Steel Detailing</h6>
			  <div class="text-small">We offer variety of structural steel detailing Services for the North American construction industry.</div>
			</a>
		  </div>
		  <div class="service col-sm-4 col-md-4" data-appear-animation="bounceInDown">
			<a href="#">
			  <div class="icon border border-error">
				<div class="livicon" data-n="brush" data-s="42" data-c="#c10841" data-hc="0"></div>
			  </div>
			  <h6 class="title">Shop Drawing</h6>
			  <div class="text-small">We offer variety of structural steel detailing Services for the North American construction industry.</div>
			</a>
		  </div>
		  <div class="service col-sm-4 col-md-4" data-appear-animation="bounceInDown">
			<a href="#">
			  <div class="icon border border-error">
				<div class="livicon" data-n="responsive" data-s="42" data-c="#c10841" data-hc="0" data-d="1600"></div>
			  </div>
			  <h6 class="title">CAD Design Services</h6>
			  <div class="text-small">We offer variety of structural steel detailing Services for the North American construction industry.</div>
			</a>
		  </div>
		  <div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	  
	  <div class="row">
	  <div class="bottom-padding col-sm-6 col-md-6">
		  <div class="progress progress-striped active">
			<div class="progress-bar progress-bar-info" style="width: 80%;">3D Modeling using Tekla structures</div>
		  </div>
		  
		  <div class="progress progress-striped active">
			<div class="progress-bar progress-bar-success" style="width: 70%;">Connection design</div>
		  </div>
		  
		  <div class="progress progress-striped">
			<div class="progress-bar" style="width: 80%;">Fabrication Drawings, Erection Drawings & Part Drawings</div>
		  </div>
		  
		  <div class="progress progress-striped active">
			<div class="progress-bar progress-bar-warning " style="width: 90%;">Checking</div>
		  </div>
		  
		</div>
		
	<div class="bottom-padding col-sm-6 col-md-6">
	    <div class="progress progress-striped active">
			<div class="progress-bar progress-bar-danger" style="width: 70%;">Misc. metals detailing</div>
		  </div>
		  <div class="progress progress-striped active">
			<div class="progress-bar progress-bar-info" style="width: 80%;">Production files like NC files, EJE files, KISS file, Fabtrol list, etc.</div>
		  </div>
		 
		  <div class="progress progress-striped active">
			<div class="progress-bar" style="width: 40%;">Material List, Assembly list, Part List, Bolt List, Drawing Index.</div>
		  </div>
		</div>
	  </div>
	</div><!-- .container -->
	
	<div class="full-width-box bottom-padding">
	  <div class="fwb-bg fwb-fixed band-12"><div class="overlay"></div></div>
	  
	  <div class="container">
		<div class="row">
		  <div class="carousel-box load" data-carousel-pagination="true" data-carousel-nav="false" data-carousel-autoplay="true">
			<div class="clearfix"></div>
			
			<div class="row">
			  <div class="carousel no-responsive">
			  	<?php foreach($result as $row){?>
				<div class="col-sm-3 col-md-3">
					<img class="replace-2x imageHeight" src="<?php echo base_url()."".$row['thumbnail']; ?>" alt="">
					<span class="shadow"></span>
				</div>
				<?php }  ?>
			  </div>
			</div>
			<div>
			  <div class="pagination switches"></div>
			</div>
		  </div>
		</div>
	  </div>
	</div><!-- .full-width-box -->
	
  </article>
</section><!-- #main -->