<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
	  parent::__construct();
	   $this->load->helper(array('url','form','array'));
       $this->load->library(array('form_validation', 'session', 'email', 'pagination'));
	   $this->load->model('index_model');
	}
	public function index()
	{
		$data['result'] = $this->index_model->select_index_portfolio();
        $this->load->view('header');
		$this->load->view('banner');
		$this->load->view('index', $data);
		$this->load->view('footer');
	}
	public function services()
	{
        $this->load->view('header');
		$this->load->view('services');
		$this->load->view('footer');
	}
	public function quality()
	{
        $this->load->view('header');
		$this->load->view('quality');
		$this->load->view('footer');
	}
	public function solutions()
	{
		$data['result'] = $this->index_model->select_index_portfolio();
        $this->load->view('header');
		$this->load->view('solutions', $data);
		$this->load->view('footer');
	}
	public function gallery()
	{
        $this->load->view('header');
		$this->load->view('gallery');
		$this->load->view('footer');
	}
	public function portfolio()
	{

	    $config = array();
        $config["base_url"] = base_url() . "welcome/example1";
        $config["total_rows"] = $this->Countries->record_count();
        $config["per_page"] = 20;
        $config["uri_segment"] = 3;

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["results"] = $this->Countries->
            fetch_countries($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();




		$data['result'] = $this->index_model->portfolio();
        $this->load->view('header');
		$this->load->view('portfolio', $data);
		$this->load->view('footer');
	}
	public function project()
	{
        $this->load->view('header');
		$this->load->view('project');
		$this->load->view('footer');
	}
	public function aboutus()
	{
        $this->load->view('header');
		$this->load->view('aboutus');
		$this->load->view('footer');
	}
	public function ourteam()
	{
        $this->load->view('header');
		$this->load->view('ourteam');
		$this->load->view('footer');
	}
	public function contactus()
	{
        $this->load->view('header');
		$this->load->view('contactus');
		$this->load->view('footer');
	}
    
	public function login()
	{
        $this->load->view('header');
		$this->load->view('login');
		$this->load->view('footer');
	}

	public function forgot()
	{
        $this->load->view('header');
		$this->load->view('forgot');
		$this->load->view('footer');
	}
	public function validate_login()
	{
		if($this->input->post('login'))
		{
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[4]');
		if ($this->form_validation->run() == FALSE)
        {
			$this->load->view('header');
            $this->load->view('login');
			$this->load->view('footer'); 
		}
		else{
		
			$data = $this->index_model->login_user($this->input->post('email'), md5($this->input->post('password')));
			if($data)
			{
				if($data['status'] == "Pending"){
					$messge = array('message' => 'Your account is not activated. wait for admin responce.','class' => 'alert alert-danger alert-dismissable fade in');
					$this->session->set_flashdata('item', $messge);
						redirect('home/login');
				}
				if($data['status'] == "Block"){
					$messge = array('message' => 'Your account is blocked. contact for admin help.','class' => 'alert alert-danger alert-dismissable fade in');
					$this->session->set_flashdata('item', $messge);
						redirect('home/login');
				}
				else{
					$this->session->user_data = $data;
					if($data['type'] == "User"){
						 redirect('user/index');
					}
					if($data['type'] == "Admin"){
						 redirect('admin/index');
					}
				}
			}
			else{
				$messge = array('message' => 'Invalid Email / password.','class' => 'alert alert-danger alert-dismissable fade in');
                $this->session->set_flashdata('item', $messge);
                    redirect('home/login');
			}
		}
		
		}
	}
    public function register()
	{
		$data['id'] = $this->index_model->selectmaxidfromuser();
		$this->load->view('header');
		$this->load->view('register', $data);
		$this->load->view('footer');
	}

	public function registeruser()
	{
		if($this->input->post('register'))
		{
		$this->form_validation->set_rules("username", "User Name", "trim|required|alpha");
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[4]');
        $this->form_validation->set_rules('securityText', 'Security Text','trim|required');
		if ($this->form_validation->run() == FALSE)
        {
			$data['id'] = $this->index_model->selectmaxidfromuser();
			$this->load->view('header');
            $this->load->view('register', $data);
			$this->load->view('footer'); 
        }
		else{
			
			if($this->index_model->email_check($this->input->post('email')))
			{
			$type = "User";
			$status = "Pending";
			
			 $userData = array(
                'emp_id' => strip_tags($this->input->post('employeeId')),
				'username' => strip_tags($this->input->post('username')),
				'password' => md5($this->input->post('password')),
				'type' => strip_tags($type),
                'email' => strip_tags($this->input->post('email')),
                'security_txt' => $this->input->post('securityText'),
                'status' => strip_tags($status)
            );
			
			if($this->index_model->insert($userData)){
				
				$data['id'] = $this->index_model->selectmaxidfromuser();
				/* $messge = array('message' => 'Your registration was successfully. Please login to your account.','class' => 'alert alert-success alert-dismissable fade in');
                $this->session->set_flashdata('item', $messge);
                    redirect('home/register'); */
				$from_email = "raushan.k@chrp-india.com";
				$to_email = $this->input->post('email');
				//Load email library
				//$this->load->library('email');
				$this->email->from($from_email, 'CHRP-INDIA');
				$this->email->to($to_email);
				$this->email->subject('Send Email Codeigniter');
				$this->email->message('The email send using codeigniter library');
				//Send mail
				if($this->email->send()){
				$messge = array('message' => 'Your registration was successfully. Please login to your account.Congragulation Email Send Successfully.','class' => 'alert alert-success alert-dismissable fade in');
                $this->session->set_flashdata('item', $messge);
                    redirect('home/register'); 
				}
				else{
					$messge = array('message' => 'Email not send.','class' => 'alert alert-danger alert-dismissable fade in');
                    $this->session->set_flashdata('item', $messge);
                    redirect('home/register'); 
				}				
			}
			else{
			$data['id'] = $this->index_model->selectmaxidfromuser();
			$messge = array('message' => 'Your registration was failed.','class' => 'alert alert-warning alert-dismissable fade in');
                $this->session->set_flashdata('item', $messge);
                    redirect('home/register');
			}
			}
			else{
				$data['id'] = $this->index_model->selectmaxidfromuser();
			$messge = array('message' => 'Email is already extis.','class' => 'alert alert-warning alert-dismissable fade in');
                $this->session->set_flashdata('item', $messge);
                    redirect('home/register');
			}
		}
		
		}
	}

	public function contactmail(){
		if($this->input->post('submit'))
		{
		$this->form_validation->set_rules("name", "Name", "trim|required");
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('phone', 'Phone', 'trim|required|min_length[10]');
        $this->form_validation->set_rules('comment', 'Comment','trim|required');
		if ($this->form_validation->run() == FALSE)
        {
			$this->load->view('header');
            $this->load->view('contactus');
			$this->load->view('footer'); 
        }
		else{
			    $name = strip_tags($this->input->post('name'));
			    $email = strip_tags($this->input->post('email'));
			    $phone = strip_tags($this->input->post('phone'));
			    $comment = strip_tags($this->input->post('comment'));
			    
			    $this->email->from('raushancodeflix@gmail.com', 'SysCAD');
				$this->email->to($email);
				//$this->email->cc('another@another-example.com');
				//$this->email->bcc('them@their-example.com');

				$this->email->subject('Contact Us');
				$this->email->message('Testing the email class.');

				if($this->email->send()){
                    $messge = array('message' => 'Email send successfully.','class' => 'alert alert-success alert-dismissable fade in');
                $this->session->set_flashdata('item', $messge);
                    redirect('home/contactus');
				}
				else{
                       $messge = array('message' => 'Email send failed.','class' => 'alert alert-danger alert-dismissable fade in');
                $this->session->set_flashdata('item', $messge);
                    redirect('home/contactus');
				} 
		}
	}
	}

}
