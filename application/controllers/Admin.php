<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
	  parent::__construct();
	  $this->load->helper(array('url','form','array'));
      $this->load->library(array('form_validation', 'session', 'email'));
	  $this->load->model('admin_model');
	}
	public function index()
	{
		$data['upload_images'] = $this->admin_model->count_upload_images();
		$data['users'] = $this->admin_model->countusers();
        $this->load->view('admin/header');
		$this->load->view('admin/index', $data);
		$this->load->view('admin/footer');
	}
	public function service(){
			$this->load->view('admin/header');
			$this->load->view('admin/service');
			$this->load->view('admin/footer');
	}
	/*.................Image Category Coding ....................*/
	public function category(){
			$this->load->view('admin/header');
			$this->load->view('admin/category');
			$this->load->view('admin/footer');
	}
	
	public function add_category(){
		if($this->input->post('submit'))
		{
		$this->form_validation->set_rules('category', 'Category', 'trim|required');
		if ($this->form_validation->run() == FALSE)
        {
			$this->load->view('admin/header');
			$this->load->view('admin/category');
			$this->load->view('admin/footer'); 
		}
		else{
			$category = array('name' =>strip_tags($this->input->post('category')));
			if($this->admin_model->get_duplicate_category($this->input->post('category'))){
                 $messge = array('message' => 'Category already exit.','class' => 'alert alert-danger alert-dismissable fade in');
                $this->session->set_flashdata('item', $messge);
                    redirect('admin/category');
			}
			else{
                  if($this->admin_model->insertcategory($category)){
					$messge = array('message' => 'Category added successfully.','class' => 'alert alert-success alert-dismissable fade in');
	                $this->session->set_flashdata('item', $messge);
	                    redirect('admin/category');
					}
					else{
						$messge = array('message' => 'Category added failed.','class' => 'alert alert-success alert-dismissable fade in');
		                $this->session->set_flashdata('item', $messge);
		                    redirect('admin/category');
					}
			}
		}
		}
	}
	public function viewcategory(){
		$data['value'] = $this->admin_model->selectcategory();
		$data['count'] = $this->admin_model->countcategory();
		$this->load->view('admin/header');
		$this->load->view('admin/viewcategory', $data);
		$this->load->view('admin/footer'); 
	}
	public function upload()
	{
		$data['category'] = $this->admin_model->selectcategory();
		$data['id'] = $this->admin_model->selectmaxidfromimage();
        $this->load->view('admin/header');
		$this->load->view('admin/upload', $data);
		$this->load->view('admin/footer');
	}
	public function uploadfile(){
	  if($this->input->post('Submit')){
		  //$dt = date('d M Y h:i a');
		  $dt = date('Y-m-d H:i:s');
		  $file_temp = $_FILES["files"]["tmp_name"];
		  $file = $_FILES["files"]["name"];
		  $type = $_FILES["files"]["type"];
		  $size = round($_FILES["files"]["size"] / 1024) . " KB";
		  $fileextension = strtolower(explode(".",$file)[1]);
		  $images = array('gif','jpg','jpeg','png');
		  list($filewidth, $fileheight) = getimagesize($file_temp);
		   
		  if(in_array($fileextension,$images)){
			  if(file_exists("./uploads/" . $_FILES["files"]["name"]))
			   {
				$messge = array('message' => 'Image already exits.','class' => 'alert alert-danger alert-dismissable fade in');
				$this->session->set_flashdata('item', $messge);	
				redirect('admin/upload');
			   }
			   else{
				  $config['upload_path']   = './uploads/'; 
				  $config['allowed_types'] = 'gif|jpg|jpeg|png'; 
				  $config['max_size']      = 2048000;
				  $this->load->library('upload', $config);
		  
		  if (!$this->upload->do_upload('files')) {
			    $data['category'] = $this->admin_model->selectcategory();
				$data['id'] = $this->admin_model->selectmaxidfromimage();
			    $data = array('error' => $this->upload->display_errors()); 
			 $this->load->view('admin/header');
			 $this->load->view('admin/upload',$data); 
			 $this->load->view('admin/footer');
		  }else { 
			$uploadedImage = $this->upload->data();
			// create thumnail coding....
			
			$source_path = './uploads/'.$uploadedImage['file_name'];
			$target_path = './uploads/thumbnail/'.$uploadedImage['file_name'];
			
			$sourcepath = 'uploads/'.$uploadedImage['file_name'];
			$thumnailpath = 'uploads/thumbnail/'.$uploadedImage['file_name'];
		    $config_manip = array(
			  'image_library' => 'gd2',
			  'source_image' => $source_path,
			  'new_image' => $target_path,
			  'maintain_ratio' => TRUE,
			  'create_thumb' => TRUE,
			  'thumb_marker' => '',
			  'width' => 370,
			  'height' => 270
            );
            $this->load->library('image_lib', $config_manip);
			$this->image_lib->resize();
			$this->image_lib->clear();
			// end create thumnail coding....
			
            $uploadData = array(
            	   "image_name" =>strip_tags($file),
            	   "image_path" =>strip_tags($sourcepath),
            	   "image_type" =>strip_tags($type),
            	   "image_size" =>strip_tags($size),
            	   "image_width" =>strip_tags($filewidth),
            	   "image_height" =>strip_tags($fileheight),
            	   "thumbnail" =>strip_tags($thumnailpath),
            	   "category" =>strip_tags($this->input->post('category')),
            	   "date" =>strip_tags($dt)
            	   );
			if($this->admin_model->insertimages($uploadData))
			{
			$messge = array('message' => 'Image Uploaded Successfully.','class' => 'alert alert-success alert-dismissable fade in');
			$this->session->set_flashdata('item', $messge);	
			redirect('admin/upload');
			}
			else{
				$messge = array('message' => 'Image Uploaded failed.','class' => 'alert alert-danger alert-dismissable fade in');
			$this->session->set_flashdata('item', $messge);	
			redirect('admin/upload');
			}
			
		  }
			   }
		  }
		  else{
			  $messge = array('message' => 'Please select jpg, jpeg, png, gif file format.','class' => 'alert alert-danger alert-dismissable fade in');
              $this->session->set_flashdata('item', $messge);
              redirect('admin/upload');
		  }
		}
	}


    /*.................User status Coding ....................*/
	public function account(){
        $data['value'] = $this->admin_model->selectusers();
		$data['count'] = $this->admin_model->countusers();
		$this->load->view('admin/header');
		$this->load->view('admin/account', $data);
		$this->load->view('admin/footer');
	}
	public function user_status(){
		$email = $this->input->get('email');
		$id = $this->input->get('id');
		$data['value'] = $this->admin_model->get_userdata($email, $id);
		$this->load->view('admin/header');
		$this->load->view('admin/user_update', $data);
		$this->load->view('admin/footer');
	}
	public function update_user_status(){
		if($this->input->post('Submit'))
		{
			$id = $this->input->post('id'); 
			$email = $this->input->post('email'); 
			
			$data = array(
			'type' => $this->input->post('userType'),
			'status' => $this->input->post('userStatus')
			);
			if($this->admin_model->update_user_status($id,$data)){
				$messge = array('message' => 'User details updated successfully.','class' => 'alert alert-success alert-dismissable fade in');
                $this->session->set_flashdata('item', $messge);
                redirect('admin/user_status?id='.$id.'&email='.$email.'');
			}
			else{
				$messge = array('message' => 'User details updated failed.','class' => 'alert alert-danger alert-dismissable fade in');
                $this->session->set_flashdata('item', $messge);
                  redirect('admin/user_status?id='.$id.'&email='.$email.'');
			}
		}
	}

	/*.................Change logo Details ....................*/
    public function changelogoimage(){
		$this->load->view('admin/header');
		$this->load->view('admin/changelogo');
		$this->load->view('admin/footer');
	}	
	 public function changelogo(){
		 if($this->input->post('submit')){
			//'allowed_types' => "gif|jpg|jpeg|png|iso|dmg|zip|rar|doc|docx|xls|xlsx|ppt|pptx|csv|ods|odt|odp|pdf|rtf|sxc|sxi|txt|exe|avi|mpeg|mp3|mp4|3gp",
				$config = array(
					'upload_path' => "./assets/img/logo/",
					'allowed_types' => "gif|jpg|png|jpeg",
					'overwrite' => TRUE,
					'max_size' => "2048000"
					);

                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('image')){
					$messge = array('message' => 'Image upload failed.','class' => 'alert alert-danger alert-dismissable fade in');
					$this->session->set_flashdata('item', $messge);
					
					$data = array('error' => $this->upload->display_errors()); 
					$this->load->view('admin/header');
					$this->load->view('admin/changelogo', $data);
					$this->load->view('admin/footer');
                }
                else{
					$uploadedImage = $this->upload->data();
					$file = $_FILES["image"]["name"];
					$sourcepath = 'assets/img/logo/'.$uploadedImage['file_name'];
					$data = array(
					        "path" =>strip_tags($sourcepath),
							"image_name" =>strip_tags($file)
					        );
						if($this->admin_model->count_logo_image()){
							if($this->admin_model->updatelogoimage($data)){
							$messge = array('message' => 'Image upload Successfully.','class' => 'alert alert-success alert-dismissable fade in');
							$this->session->set_flashdata('item', $messge);
							$this->load->view('admin/header');
							$this->load->view('admin/changelogo');
							$this->load->view('admin/footer');
							}
							else{
							$messge = array('message' => 'Image upload failed.','class' => 'alert alert-danger alert-dismissable fade in');
								$this->session->set_flashdata('item', $messge);
								$this->load->view('admin/header');
								$this->load->view('admin/changelogo');
								$this->load->view('admin/footer');
							}
						}
                        else{
                             if($this->admin_model->insertlogoimage($data)){
							$messge = array('message' => 'Image upload Successfully.','class' => 'alert alert-success alert-dismissable fade in');
							$this->session->set_flashdata('item', $messge);
							$this->load->view('admin/header');
							$this->load->view('admin/changelogo');
							$this->load->view('admin/footer');
							}
							else{
							$messge = array('message' => 'Image upload failed.','class' => 'alert alert-danger alert-dismissable fade in');
								$this->session->set_flashdata('item', $messge);
								$this->load->view('admin/header');
								$this->load->view('admin/changelogo');
								$this->load->view('admin/footer');
							}
						}													
                }
		 }
	}

	/*.................Title Details ....................*/
	public function title(){
		$this->load->view('admin/header');
		$this->load->view('admin/title');
		$this->load->view('admin/footer');
	}
	public function insert_title(){
		 if($this->input->post('submit')){
	$this->form_validation->set_rules('title', 'Title', 'trim|required');
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('admin/header');
			$this->load->view('admin/title');
			$this->load->view('admin/footer'); 
		}
		else{
$data = array("name" => strip_tags($this->input->post('title')));
              if($this->admin_model->count_title()){
				 if($this->admin_model->sql_update_title($data)){
				$messge = array('message' => 'Title added successfully..','class' => 'alert alert-success alert-dismissable fade in');
				$this->session->set_flashdata('item', $messge);
				$this->load->view('admin/header');
				$this->load->view('admin/title');
				$this->load->view('admin/footer');
			}
			else{
				$messge = array('message' => 'Title added failed.','class' => 'alert alert-danger alert-dismissable fade in');
				$this->session->set_flashdata('item', $messge);
				$this->load->view('admin/header');
				$this->load->view('admin/title');
				$this->load->view('admin/footer');
			}
			  }
			  else{
				  if($this->admin_model->sql_insert_title($data)){
				$messge = array('message' => 'Title added successfully..','class' => 'alert alert-success alert-dismissable fade in');
				$this->session->set_flashdata('item', $messge);
				$this->load->view('admin/header');
				$this->load->view('admin/title');
				$this->load->view('admin/footer');
			}
			else{
				$messge = array('message' => 'Title added failed.','class' => 'alert alert-danger alert-dismissable fade in');
				$this->session->set_flashdata('item', $messge);
				$this->load->view('admin/header');
				$this->load->view('admin/title');
				$this->load->view('admin/footer');
			}
			  }
		}
		 }
	}

	/*.................Change password  Coding ....................*/
	public function check_oldpassword_avalibility(){
		if(!empty($this->input->post('email')) && !empty($this->input->post('password'))){
			$email = $this->input->post('email');
			$password = md5($this->input->post('password'));
			if($this->admin_model->check_old_password($email, $password)){
				echo "<span style='color:green;' class='fa fa-check'></span>";
			}
			else{
			    echo "<span style='color:red;'><i class='fa fa-close'></i> Passwords entered do not match.</span>";
			}
		}
		else{
			echo "Empty field";
		}
	}
	public function change_password(){
		$this->load->view('admin/header');
		$this->load->view('admin/changepassword');
		$this->load->view('admin/footer');
	}
	public function changepassword(){
		if($this->input->post('submit')){
		$this->form_validation->set_rules('oldpassword', 'Old password', 'trim|required');
		$this->form_validation->set_rules('newpassword', 'New password', 'trim|required|min_length[4]');
		$this->form_validation->set_rules('confirmpassword', 'Confirm password', 'trim|required|min_length[4]');
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('admin/header');
			$this->load->view('admin/changepassword');
			$this->load->view('admin/footer');
		}
		else{
$data = array("password" => md5($this->input->post('confirmpassword')));
            $email = $this->input->post('email');
			$oldpassword = md5($this->input->post('oldpassword'));
			$newpassword = md5($this->input->post('newpassword'));
			$confirmpassword = md5($this->input->post('confirmpassword'));
			if($newpassword == $confirmpassword)
			{
				if($this->admin_model->check_old_password($email, $oldpassword)){
				   if($this->admin_model->update_password($data)){
						$messge = array('message' => 'Password updated successfully.','class' => 'alert alert-success alert-dismissable fade in');
						$this->session->set_flashdata('item', $messge);
						$this->load->view('admin/header');
						$this->load->view('admin/changepassword');
						$this->load->view('admin/footer');
					}
					else{
						$messge = array('message' => 'Password updation failed.','class' => 'alert alert-danger alert-dismissable fade in');
						$this->session->set_flashdata('item', $messge);
						$this->load->view('admin/header');
						$this->load->view('admin/changepassword');
						$this->load->view('admin/footer');
					}
				}
				else{
					$messge = array('message' => 'Please enter correct old password.','class' => 'alert alert-danger alert-dismissable fade in');
					$this->session->set_flashdata('item', $messge);
					$this->load->view('admin/header');
					$this->load->view('admin/changepassword');
					$this->load->view('admin/footer');
				}
			}
			else{
				$messge = array('message' => 'New Password and Confirm password not match.','class' => 'alert alert-danger alert-dismissable fade in');
				$this->session->set_flashdata('item', $messge);
				$this->load->view('admin/header');
				$this->load->view('admin/changepassword');
				$this->load->view('admin/footer');
			}
		}
		}
	}
	public function logout()
	{
        $this->session->sess_destroy();
	    redirect('home/index');
	}

}
?>
